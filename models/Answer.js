var connection= require('../mongooseConnection');
var mongoose=connection.mongoose;
var Schema=mongoose.Schema;
var deepPopulate=connection.deepPopulate;

//Schema Declarations
var AnswerSchema = Schema({
        choice: {type: String, required: true, trim: true},
        refToQuestion: {type: Schema.Types.ObjectId, ref: 'Question'},
        hours:{type: Number, trim: true},
        baseRate:{type: Number, trim: true},
        range:[{type: Number, trim: true}],
    },
    {   timestamps: true,
        collection: "Answer"});


AnswerSchema.plugin(deepPopulate,{});

var Answer = mongoose.model('Answer', AnswerSchema);
module.exports=Answer;