var connection = require('../mongooseConnection');
var mongoose = connection.mongoose;
var Schema = mongoose.Schema;
var deepPopulate = connection.deepPopulate;

//Schema Declarations
var OrderSchema = Schema({

        orderNo: {type: Number, require: true},
        answerToQuestion: [{type: Schema.Types.ObjectId}],
        refToServiceProvider: {type: Schema.Types.ObjectId, ref: 'ServiceProvider'},
        orderDescription: {type: String, trim: true},
        status: {type: String, trim: true, default: 'unfulfilled'},
        employeeId: {type: Schema.Types.ObjectId, ref: 'ServiceProvider'},
        serviceDate: {type: Date, default: null},
        serviceTimeSlot: {type: String, default: null},
        serviceDateAndTime: {type: String, default: null},
        refToServiceType: {type: Schema.Types.ObjectId, ref: 'ServiceType'},
        refToUser: {type: Schema.Types.ObjectId, ref: 'User'},
        addressLat: {type: String, trim: true},
        addressLong: {type: String, trim: true},
        addressLatEnd: {type: String, trim: true},
        addressLongEnd: {type: String, trim: true},
        addressBuildingNumber: {type: String, trim: true},
        addresshouseNumber: {type: String, trim: true},
        addressfloor: {type: String, trim: true},
        addressLocationName: {type: String, trim: true},
        addressBuildingNumberEnd: {type: String, trim: true},
        addresshouseNumberEnd: {type: String, trim: true},
        addressfloorEnd: {type: String, trim: true},
        addressLocationNameEnd: {type: String, trim: true},
        imageUrl: [{type: String}],
        customerFeedback: [{type: String, trim: true}],
        orderRating: {type: Number, trim: true},
        employeeStartTime: {type: Date},
        employeeStartEndTime: {type: Date},
        employeeStartLocationLat: {type: String, trim: true},
        employeeStartLocationLong: {type: String, trim: true},
        employeeStartLocationEndLat: {type: String, trim: true},
        employeeStartLocationEndLong: {type: String, trim: true},
        estimateCost: {type: String, min: 0, trim: true},
        request: {type: String, min: 0, trim: true}

    },
    {
        timestamps: true,
        collection: "Order"
    });

OrderSchema.plugin(deepPopulate, {});

var Order = mongoose.model('Order', OrderSchema);
module.exports = Order;
