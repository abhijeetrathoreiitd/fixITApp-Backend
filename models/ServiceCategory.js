var connection= require('../mongooseConnection');
var mongoose=connection.mongoose;
var Schema=mongoose.Schema;
var deepPopulate=connection.deepPopulate;



//Schema Declarations
var ServiceCategorySchema = Schema({
        category: {type: String, required: true, trim: true,unique: true},
        refToServiceType: [{type: Schema.Types.ObjectId, ref: 'ServiceType'}],
    },
    {
        timestamps: true,
        collection: "ServiceCategory"});


ServiceCategorySchema.plugin(deepPopulate,{});

var ServiceCategory = mongoose.model('ServiceCategory', ServiceCategorySchema);

module.exports=ServiceCategory;