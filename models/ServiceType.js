var connection= require('../mongooseConnection');
var mongoose=connection.mongoose;
var Schema=mongoose.Schema;
var deepPopulate=connection.deepPopulate;



//Schema Declarations

var ServiceTypeSchema = Schema({
        serviceType: {type: String, required: true, trim: true,unique: true},
        refToServiceCategory: [{type: Schema.Types.ObjectId, ref: 'ServiceCategory'}],
        refToQuestionnaire: {type: Schema.Types.ObjectId, ref: 'Questionnaire'},
        refToServiceProvider: [{type: Schema.Types.ObjectId, ref: 'ServiceProvider'}],
        cost:{type: String},
        disabled:{type: Boolean,default:false},
        order: {type: Number}//maintains the sequence


    },
    {   timestamps: true,
        collection: "ServiceType"});


ServiceTypeSchema.plugin(deepPopulate,{});

var ServiceType = mongoose.model('ServiceType', ServiceTypeSchema);

module.exports=ServiceType;