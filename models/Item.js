var connection= require('../mongooseConnection');
var mongoose=connection.mongoose;
var Schema=mongoose.Schema;
var deepPopulate=connection.deepPopulate;

//Schema Declarations

var ItemSchema = Schema({
        refToServiceMenu: {type: Schema.Types.ObjectId, ref: 'ServiceMenu'},
        title: {type: String, required: true, trim: true},
        priceEstimate: {type: String, required: true, trim: true},
        serviceTypeId: {type: String, required: true, trim: true},

    },
    {   timestamps: true,
        collection: "Item"});

ItemSchema.plugin(deepPopulate,{});

var Item = mongoose.model('Item', ItemSchema);
module.exports=Item;