var connection= require('../mongooseConnection');
var mongoose=connection.mongoose;
var Schema=mongoose.Schema;
var deepPopulate=connection.deepPopulate;

//Schema Declarations
var RatingSchema = Schema({
        AverageRating: {type: Number, required: true, default:0,trim: true},
        NumberOfRating: {type: Number, required: true,default:0, trim: true},
        refToServiceProvider: {type: Schema.Types.ObjectId, ref: 'ServiceProvider'},
    },
    {   timestamps: true,
        collection: "Rating"});


RatingSchema.plugin(deepPopulate,{});

var Rating = mongoose.model('Rating', RatingSchema);
module.exports=Rating;