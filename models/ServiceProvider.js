var connection = require('../mongooseConnection');
var mongoose = connection.mongoose;
var Schema = mongoose.Schema;
var deepPopulate = connection.deepPopulate;
//Schema Declarations


var ServiceProviderSchema = Schema({

        firstName: {type: String, required: true, trim: true},
        lastName: {type: String, required: true, trim: true},
        organization_name: {type: String, trim: true},
        phoneNumber: {type: String, trim: true},
        photo: {type: String, trim: true},
        accountActive: {type: Boolean, trim: true, default: true},
        description: {type: String, trim: true},
        refToServiceType: [{type: Schema.Types.ObjectId, ref: 'ServiceType'}],
        refToUser: {type: Schema.Types.ObjectId, ref: 'User'},
        refToServiceMenu: {type: Schema.Types.ObjectId, ref: 'ServiceMenu'},
        refToRating: {type: Schema.Types.ObjectId, ref: 'Rating'},
        imageURL: {type: String},
        serviceProviderId: {type: String, trim: true},//for employees only
        accountType: {type: String, trim: true}//for employees and service provider only
    },
    {
        timestamps: true,
        collection: "ServiceProvider"
    });
ServiceProviderSchema.plugin(deepPopulate, {});

var ServiceProvider = mongoose.model('ServiceProvider', ServiceProviderSchema);
module.exports = ServiceProvider;