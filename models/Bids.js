var connection= require('../mongooseConnection');
var mongoose=connection.mongoose;
var Schema=mongoose.Schema;
var deepPopulate=connection.deepPopulate;

//Schema Declarations

var BidsSchema = Schema({
        refToServiceProvider: {type: Schema.Types.ObjectId, ref: 'ServiceProvider'},
        bidding: {type: String, trim: true},
        bidWon: {type: Boolean, required: true, trim: true, default:false},
        bidOver: {type: Boolean, required: true, trim: true, default:false},
        refToOrder: {type: Schema.Types.ObjectId, ref: 'Order'},
        refToUser: {type: Schema.Types.ObjectId, ref: 'User'},
    },
    {   timestamps: true,
        collection: "Bids"});

BidsSchema.plugin(deepPopulate,{});

var Bids = mongoose.model('Bids', BidsSchema);
module.exports=Bids;