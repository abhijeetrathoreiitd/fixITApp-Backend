
var connection= require('../mongooseConnection');
var mongoose=connection.mongoose;
var Schema=mongoose.Schema;
var deepPopulate=connection.deepPopulate;

var ServiceMenuSchema = Schema({
        refToServiceProvider: {type: Schema.Types.ObjectId, ref: 'ServiceProvider'},
        refToItem: [{type: Schema.Types.ObjectId, ref: 'Item'}]
    },
    {   timestamps: true,
        collection: "ServiceMenu"});


ServiceMenuSchema.plugin(deepPopulate,{});

var ServiceMenu = mongoose.model('ServiceMenu', ServiceMenuSchema);
module.exports=ServiceMenu;

