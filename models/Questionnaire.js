var connection= require('../mongooseConnection');
var mongoose=connection.mongoose;
var Schema=mongoose.Schema;
var deepPopulate=connection.deepPopulate;

//Schema Declarations
var QuestionnaireSchema = Schema({
        refToServiceType: {type: Schema.Types.ObjectId, ref: 'ServiceType',unique: true},
        refToQuestion: [{type: Schema.Types.ObjectId, ref: 'Question'}],
    },
    {   timestamps: true,
        collection: "Questionnaire"});


QuestionnaireSchema.plugin(deepPopulate,{});

var Questionnaire = mongoose.model('Questionnaire', QuestionnaireSchema);
module.exports=Questionnaire;