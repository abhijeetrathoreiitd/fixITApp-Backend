
var connection= require('../mongooseConnection');
var mongoose=connection.mongoose;
var Schema=mongoose.Schema;
var deepPopulate=connection.deepPopulate;
//Schema Declarations
var UserSchema = Schema({
        firstName: {type: String, required: true, trim: true},
        lastName: {type: String,  trim: true},
        photo: {type: String, trim: true},
        email: {type: String, required: true, trim: true},
        phoneNumber: {type: String,  trim: true, minlength: 8, maxlength: 8},
        deviceToken: {type: String,  trim: true},
        fbUserId: {type: String,  trim: true},
        password: {type: String,  trim: true},
        enabled: {type: Boolean,required: true, default:false,trim: true},
        verified: {type: Boolean,required: true,default:false,trim: true},
        imageURL: {type: String},
        refToAddress: {type: Schema.Types.ObjectId, ref: 'Address'},
        serviceProviderId: {type: Schema.Types.ObjectId,trim:true},
        accountType: {type: String,  required: true, trim: true}//PASS accountType as serviceProvider for registering a serviceProvider.
    },
    {
            timestamps:true,
            collection: "User"});

//Deep Population Declarations
UserSchema.plugin(deepPopulate,{});

//Model Declarations
var User = mongoose.model('User', UserSchema);

module.exports=User;