var connection= require('../mongooseConnection');
var mongoose=connection.mongoose;
var Schema=mongoose.Schema;
var deepPopulate=connection.deepPopulate;

//Schema Declarations
var QuestionSchema = Schema({
        question: {type: String, required: true, trim: true},
        refToQuestionnaire: {type: Schema.Types.ObjectId, ref: 'Questionnaire'},
        questionType:{type: String, required: true, trim: true},//single, multi,text,multple-with-text
        refToAnswer: [{type: Schema.Types.ObjectId, ref: 'Answer'}],
    },
    {   timestamps: true,
        collection: "Question"});


QuestionSchema.plugin(deepPopulate,{});

var Question = mongoose.model('Question', QuestionSchema);
module.exports=Question;