
var connection= require('../mongooseConnection');
var mongoose=connection.mongoose;
var Schema=mongoose.Schema;
var deepPopulate=connection.deepPopulate;
//Schema Declarations

var AddressSchema = Schema({
        refToUser: {type: Schema.Types.ObjectId, ref: 'User'},
        description: {type: String, trim: true},
        addressLat: {type: String,trim:true},
        addressLong: {type: String,trim:true},
        addressBuildingNumber: {type: String,trim:true},
        addresshouseNumber: {type: String,trim:true},
        addressfloor: {type: String,trim:true},
        addressLocationName: {type: String,trim:true},

    },
    {
        timestamps: true,
        collection: "Address"});


AddressSchema.plugin(deepPopulate,{});

var Address = mongoose.model('Address', AddressSchema);
module.exports=Address;