'use strict';
// Declare app level module which depends on filters, and services

var app = angular.module('myApp', [
    'ngRoute', 'angular.filter', 'ngResource', 'ngFileUpload'
])

app.config(function ($routeProvider, $locationProvider, $httpProvider) {
    $httpProvider.defaults.useXDomain = true;
    delete $httpProvider.defaults.headers.common['X-Requested-With'];
    $httpProvider.defaults.useXDomain = true;
    delete $httpProvider.defaults.headers.common['X-Requested-With'];
    $httpProvider.interceptors.push(function ($q, $location) {
        return {
            'responseError': function (rejection) {
                if (rejection.status == 302) {
                    if (JSON.parse(rejection.data).redirect == 'spAllBids') {
                        $location.path('/spallbids');
                    }
                    else if (JSON.parse(rejection.data).redirect == 'allOrders') {
                        $location.path('/allorders');
                    }
                    else if (JSON.parse(rejection.data).redirect == 'spLogin') {
                        $location.path('/splogin');
                    }
                }
                return $q.reject(rejection);
            }
        };
    });

    $routeProvider
        .when('/addCategory', {
            templateUrl: 'partials/addCategory',
            controller: 'addCategoryCtrl',
            controllerAs: 'category'
        })
        .when('/addService', {
            templateUrl: 'partials/addService',
            controller: 'addServiceCtrl',
            controllerAs: 'service'
        })
        .when('/splogin', {
            templateUrl: 'partials/SPlogin',
            controller: 'SPLoginCtrl',
            controllerAs: 'SPLoginCtrl'
        })
        .when('/adminlogin', {
            templateUrl: 'partials/adminLogin',
            controller: 'adminLoginCtrl',
            controllerAs: 'adminLoginCtrl'
        })
        .when('/serviceproviders', {
            templateUrl: 'partials/serviceProvider',
            controller: 'adminServiceProviderList',
            controllerAs: 'SPCtrl'
        })
        .when('/orders', {
            templateUrl: 'partials/orders',
            controller: 'orderCtrl',
            controllerAs: 'orders'
        })
        .when('/sporders', {
            templateUrl: 'partials/spOrder',
            controller: 'SPOrderCtrl',
            controllerAs: 'SPOrderCtrl'
        })
        .when('/employees', {
            templateUrl: 'partials/employees',
            controller: 'employeeCtrl',
            controllerAs: 'empCtrl'
        })
        .when('/registerserviceprovider', {
            templateUrl: 'partials/registerServiceProvider',
            controller: 'registerServiceProvider',
            controllerAs: 'RSPCtrl'
        })
        .when('/editserviceproviderdetails', {
            templateUrl: 'partials/editServiceProviderDetails',
            controller: 'editServiceProviderDetails',
            controllerAs: 'ESPCtrl'
        })
        .when('/registeremployee', {
            templateUrl: 'partials/registerEmployee',
            controller: 'registerEmployeeCtrl',
            controllerAs: 'RECtrl'
        })
        .when('/spallbids', {
            templateUrl: 'partials/spAllBids',
            controller: 'spAllBidsCtrl',
            controllerAs: 'spAllBids'
        })
        .when('/customerlist', {
            templateUrl: 'partials/customerList',
            controller: 'customerListCtrl',
            controllerAs: 'customerList'
        })
        .otherwise({
            redirectTo: '/serviceproviders'
        });
    $locationProvider.html5Mode(true);
})
    .run(function ($http, $rootScope, LoggedInUser, Time, currentTime, $location) {


        Time.get(function (data) {
            currentTime.timeNow = new Date(new Date(new Date(new Date(data.currentTime).setHours(0)).setMinutes(0)).setSeconds(0)).toISOString();
            currentTime.tomorrow = new Date(new Date(currentTime.timeNow).getTime() + 24 * 60 * 60 * 1000).toISOString();
            currentTime.startWeek = new Date(new Date(currentTime.timeNow).getTime() - new Date(currentTime.timeNow).getDay() * 24 * 60 * 60 * 1000).toISOString();
            currentTime.endWeek = new Date(new Date(currentTime.timeNow).getTime() + (6 - new Date(currentTime.timeNow).getDay()) * 24 * 60 * 60 * 1000).toISOString();

            console.log(currentTime);
            ;
        }, function (err) {
            console.log(err);
        });
        LoggedInUser.get(function (data) {
            console.log(data);
            if (data.refToUser.accountType == 'serviceProvider')
                $rootScope.loggedInSP = data._id;
        }, function (err) {
            console.log(err);
        })


        $rootScope.showHighlightedTab = function (tab) {
            console.log(tab)
            switch (tab) {
                case 'orders':
                    $rootScope.pageHeading = 'All Jobs';
                    $rootScope.pageSubheading = 'Click an order for details';
                    $rootScope.activeTab = 'orders';
                    break;

                case 'sporders':
                    $rootScope.pageHeading = 'All Jobs';
                    $rootScope.pageSubheading = 'Click an order for details';
                    $rootScope.activeTab = 'sporders';
                    break;
                case 'serviceproviders':
                    $rootScope.pageHeading = 'Service Providers';
                    $rootScope.pageSubheading = 'Click a provider for details';
                    $rootScope.activeTab = 'serviceproviders';
                    break;
                case 'registerserviceprovider':
                    $rootScope.pageHeading = 'Add a Service Provider';
                    $rootScope.pageSubheading = 'Fill all details and submit';
                    $rootScope.activeTab = 'registerserviceprovider';
                    break;
                case 'employees':
                    $rootScope.pageHeading = 'All Employees';
                    $rootScope.pageSubheading = 'See and edit employee details';
                    $rootScope.activeTab = 'employees';
                    break;
                case 'spallbids':
                    $rootScope.pageHeading = 'New Jobs';
                    $rootScope.pageSubheading = 'Click a job for details';
                    $rootScope.activeTab = 'spallbids';
                    break;
                case 'registeremployee':
                    $rootScope.pageHeading = 'Add Employee';
                    $rootScope.pageSubheading = 'Fill all details and submit';
                    $rootScope.activeTab = 'registeremployee';
                    break;
                case 'editserviceproviderdetails':
                    $rootScope.pageHeading = 'My Account';
                    $rootScope.pageSubheading = 'Edit your profile details';
                    $rootScope.activeTab = 'editserviceproviderdetails';
                    break;
                case 'customerlist':
                    $rootScope.pageHeading = 'Customer List';
                    $rootScope.pageSubheading = 'See customer details';
                    $rootScope.activeTab = 'customerlist';
                    break;
            }
        }
        $rootScope.showHighlightedTab($location.path().replace('/', ''));

    })
    .controller('addCategoryCtrl', addCategoryCtrl)
    .controller('editServiceProviderDetails', editServiceProviderDetails)
    .controller('addServiceCtrl', addServiceCtrl)
    .controller('SPLoginCtrl', SPLoginCtrl)
    .controller('adminLoginCtrl', adminLoginCtrl)
    .controller('adminServiceProviderList', adminServiceProviderList)
    .controller('orderCtrl', orderCtrl)
    .controller('registerServiceProvider', registerServiceProvider)
    .controller('registerEmployeeCtrl', registerEmployeeCtrl)
    .controller('employeeCtrl', employeeCtrl)
    .controller('SPOrderCtrl', SPOrderCtrl)
    .controller('headerCtrl', headerCtrl)
    .controller('innerHeaderCtrl', innerHeaderCtrl)
    .controller('spAllBidsCtrl', spAllBidsCtrl)
    .controller('customerListCtrl', customerListCtrl)

function headerCtrl($rootScope) {
    $rootScope.enter = false;
}
function innerHeaderCtrl(Logout, $location) {
    var innerHeader = this;
    innerHeader.logout = function () {
        Logout.get(function (data) {
            $location.path('/splogin');
        }, function (err) {
            console.log(err);
        })
    }
}
function addCategoryCtrl($timeout, Category) {
    var category = this;
    category.newCategory = {};
    category.saved = false;

    category.addCategory = function () {
        Category.save({
            category: category.newCategory.category
        }, function (data) {
            //category.newCategory={};
            category.saved = true;


            $timeout(function () {

                category.saved = false;
            }, 2000)

        }, function (err) {
            console.log(err);
            category.error = true;
            $timeout(function () {

                category.error = false;
            }, 2000)
        });
    }
}
function addServiceCtrl($timeout, Category, Service) {
    var service = this;
    service.newService = {};
    service.saved = false;
    Category.query(function (data) {
        service.newService.category = [];
        _.each(data, function (d) {
            service.newService.category.push({_id: d._id, category: d.category});

        })


    }, function (err) {
        console.log(err);
        $timeout(function () {

        }, 2000)
    })
    service.addServiceType = function () {
        Service.save({
            service: service.newService.service,
            serviceCategoryId: service.newService.category[0]._id
        }, function (data) {
            service.saved = true;


            $timeout(function () {

                service.saved = false;
            }, 2000)

        }, function (err) {
            console.log(err);
            service.error = true;
            $timeout(function () {

                service.error = false;
            }, 2000)
        });
    }
}
function SPLoginCtrl(Login, $location, LoggedInUser, $rootScope, User, $timeout) {


    var SPLoginCtrl = this;
    SPLoginCtrl.user = {};
    SPLoginCtrl.loginError = false;
    SPLoginCtrl.login = function () {


        var loginPromise = Login.save({
            email: SPLoginCtrl.user.email,
            password: SPLoginCtrl.user.password,
            accountType: 'serviceProvider'
        }, function (loginData) {
            console.log('successful login')
            console.log(loginData);
        }, function (err) {
            console.log('invalid ID Password');
            SPLoginCtrl.loginError = true;
            $timeout(function () {
                SPLoginCtrl.loginError = false;
            }, 3000);

        });


        loginPromise['$promise'].then(function () {

            console.log('get info')
            LoggedInUser.get(function (data) {
                console.log('loggedInUSer')
                console.log(data)

                if (data.refToUser.accountType == 'serviceProvider') {
                    $rootScope.loggedInSP = data._id;
                    $rootScope.showHighlightedTab('spallbids');
                    $location.path('/spallbids');
                }
            }, function (err) {
                console.log(err);
            })
        })

    }

    SPLoginCtrl.forgotPassword = function () {
        BootstrapDialog.show({
            title: 'Password Recovery',
            message: $('<input type="email" class="form-control" placeholder="Enter Email Id" >'),
            buttons: [{
                label: 'Recover Password',
                cssClass: 'btn-primary',
                hotkey: 13, // Enter.
                action: function (dialogRef) {
                    var emailForForgottenAccount = dialogRef.getModalBody().find('input').val();
                    console.log(emailForForgottenAccount);
                    User.forgotPassword({email: emailForForgottenAccount}, function (data) {
                        var message;
                        dialogRef.close();
                        if (data.data == "done")
                            message = 'New password has been sent on your email id'
                        else
                            message = 'Not a registered email id'
                        $timeout(function () {
                            BootstrapDialog.show({
                                message: message,
                                cssClass: 'login-dialog'
                            });
                        }, 300);


                    }, function (err) {
                        console.log(err);
                    })
                }
            }]
        });
    };


}
function adminLoginCtrl($location, Login, $rootScope, $timeout, User) {

    var adminLoginCtrl = this;
    adminLoginCtrl.user = {}
    adminLoginCtrl.loginError = false;
    adminLoginCtrl.login = function () {

        Login.save({
            email: adminLoginCtrl.user.email,
            password: adminLoginCtrl.user.password,
            accountType: 'admin'
        }, function (data) {

            $rootScope.showHighlightedTab('orders');
            $location.path('/orders');
        }, function (err) {

            console.log('invalid ID Password');
            adminLoginCtrl.loginError = true;
            $timeout(function () {
                adminLoginCtrl.loginError = false;
            }, 3000);

        });

    }

    adminLoginCtrl.forgotPassword = function () {
        BootstrapDialog.show({
            title: 'Password Recovery',
            message: $('<input type="email" class="form-control" placeholder="Enter Email Id" >'),
            buttons: [{
                label: 'Recover Password',
                cssClass: 'btn-primary',
                hotkey: 13, // Enter.
                action: function (dialogRef) {
                    var emailForForgottenAccount = dialogRef.getModalBody().find('input').val();
                    console.log(emailForForgottenAccount);
                    User.forgotPassword({email: emailForForgottenAccount}, function (data) {
                        var message;
                        dialogRef.close();
                        if (data.data == "done")
                            message = 'New password has been sent on your email id'
                        else
                            message = 'Not a registered email id'
                        $timeout(function () {
                            BootstrapDialog.show({
                                message: message,
                                cssClass: 'login-dialog'
                            });
                        }, 300);


                    }, function (err) {
                        console.log(err);
                    })
                }
            }]
        });
    };


}
function adminServiceProviderList($timeout, ServiceProvider) {

    var SPCtrl = this;
    SPCtrl.clicked = '';

    SPCtrl.showProviderInfo = false;
    SPCtrl.showProvider = true;
    ServiceProvider.query(function (data) {


        SPCtrl.allServiceProviders = [];

        _.each(data, function (provider) {
            var localProvider = {};
            localProvider.providerId = provider._id;
            localProvider.name = provider.firstName + ' ' + provider.lastName;
            localProvider.description = provider.description;
            localProvider.photo = provider.refToUser._id;
            localProvider.totalRating = provider.refToRating.NumberOfRating;
            localProvider.averageRating = provider.refToRating.AverageRating;
            localProvider.accountActive = provider.accountActive;
            localProvider.imageURL = provider.imageURL;

            var localServiceOffered = [];
            _.each(provider.refToServiceType, function (service) {

                if (provider.refToServiceMenu.refToItem.length != 0)
                //localProvider.menuItem=provider.refToServiceMenu.refToItem;
                {
                    _.each(provider.refToServiceMenu.refToItem, function (item) {
                        if (service._id == item.serviceTypeId)
                            localServiceOffered.push({
                                "serviceTypeId": service._id,
                                "serviceType": service.serviceType,
                                "menu": {"priceEstimate": item.priceEstimate, "title": item.title}
                            })
                    })
                }
                else {

                    localServiceOffered.push({"serviceTypeId": service._id, "serviceType": service.serviceType})
                }

            })
            localProvider.serviceOffered = localServiceOffered;
            SPCtrl.allServiceProviders.push(localProvider)
        })


    }, function (err) {
        $timeout(function () {
        }, 2000)
    })

    SPCtrl.openProviderInfo = function (provider) {

        SPCtrl.showProvider = true;
        SPCtrl.clicked = provider.name;
        SPCtrl.showProviderInfo = true;
        SPCtrl.veiwProvider = provider;
    }
    //
    //SPCtrl.deleteUser = function (service) {
    //    ServiceProvider.update({serviceProvider: service}, function (data) {
    //        SPCtrl.allServiceProviders = data;
    //
    //    }, function (err) {
    //        $timeout(function () {
    //        }, 2000)
    //    })
    //}


    SPCtrl.disableEmployee = function (employee) {
        console.log(employee)
        employee.accountActive = !employee.accountActive
        ServiceProvider.updateAccountStatus
        ({refToSP: employee.providerId, disable: employee.accountActive},
            function (data) {

                console.log(data);
            }, function (err) {
                console.log(err);
            })
    }

    SPCtrl.getAllEmployee = function (SPid) {
        SPCtrl.showProvider = false;
        ServiceProvider.allEmployee({refToSP: SPid}, function (data) {
            SPCtrl.allEmployees = [];

            _.each(data, function (provider) {
                var localProvider = {};
                localProvider.providerId = provider._id;
                localProvider.name = provider.firstName + ' ' + provider.lastName;
                localProvider.description = provider.description;
                localProvider.photo = provider.refToUser._id;
                localProvider.totalRating = provider.refToRating.NumberOfRating;
                localProvider.averageRating = provider.refToRating.AverageRating;
                localProvider.accountActive = provider.accountActive;

                var localServiceOffered = [];
                _.each(provider.refToServiceType, function (service) {

                    if (provider.refToServiceMenu.refToItem.length != 0)
                    //localProvider.menuItem=provider.refToServiceMenu.refToItem;
                    {
                        _.each(provider.refToServiceMenu.refToItem, function (item) {
                            if (service._id == item.serviceTypeId)
                                localServiceOffered.push({
                                    "serviceTypeId": service._id,
                                    "serviceType": service.serviceType,
                                    "menu": {"priceEstimate": item.priceEstimate, "title": item.title}
                                })
                        })
                    }
                    else {

                        localServiceOffered.push({"serviceTypeId": service._id, "serviceType": service.serviceType})
                    }

                })
                localProvider.serviceOffered = localServiceOffered;
                SPCtrl.allEmployees.push(localProvider);
                console.log(SPCtrl.allEmployees)
            })


        }, function (err) {
            $timeout(function () {
            }, 2000)
        })
    }

}
function orderCtrl($timeout, Order, Bid, $rootScope, $location) {

    $rootScope.showHighlightedTab($location.path().replace('/', ''))
    var orders = this;
    orders.clicked = '';
    orders.idSelected = null;
    orders.reverse = true;
    orders.sortBy = function (sortBy) {
        orders.reverse = (orders.sortOrderBy === sortBy) ? !orders.reverse : false;
        orders.sortOrderBy = sortBy;
    }
    orders.sortOrderBy = 'orderNo';
    orders.filterByCategory = 'all';
    orders.filterByServiceDate = 'all';
    orders.filterByStatus = 'all';


    orders.parentCategory = '';
    orders.allOrders = [];

    var limit = 10, skip = 0;
    orders.fetchOrders = function () {
        Order.query({limit: limit, skip: skip}, function (data) {
            skip = skip + data.length;

            _.each(data, function (ord) {

                //inserting the code for timeslot for sorting purpose
                var timeSlotCode;
                switch (ord.serviceTimeSlot) {

                    case 'Morning (8AM-12PM)':
                        timeSlotCode = 1;
                        break;

                    case 'Afternoon (1PM-4PM)':

                        timeSlotCode = 2;
                        break;
                    case 'Evening (4PM-7PM)':

                        timeSlotCode = 3;
                        break;
                }
                var localOrder = {};
                var localQuestion = [];
                localOrder._id = ord._id;
                localOrder.orderNo = ord.orderNo;
                if (ord.refToServiceType.refToServiceCategory == '579735918c70e9cc338be339') {
                    localOrder.serviceCategory = 'Moving';
                }
                else if (ord.refToServiceType.refToServiceCategory == '57974a17de6c152c3a191dee') {
                    localOrder.serviceCategory = 'Cleaning';
                }
                else {
                    localOrder.serviceCategory = 'Handyman';
                }
                localOrder.serviceType = ord.refToServiceType.serviceType;
                localOrder.serviceTypeId = ord.refToServiceType._id;
                _.each(ord.refToServiceType.refToQuestionnaire.refToQuestion, function (ques) {
                    console.log(ord.refToServiceType.refToQuestionnaire.refToQuestion)
                    var localAnswer = [];
                    _.each(ques.refToAnswer, function (ans) {
                        _.each(ord.answerToQuestion, function (useranswer) {
                            if (ans._id.toString() == useranswer.toString()) {
                                localAnswer.push({'answerId': ans._id, 'answer': ans.choice});
                            }
                        })
                    })
                    localQuestion.push({"questionId": ques._id, "question": ques.question, "answer": localAnswer})
                })
                console.log(ord)
                localOrder.questions = localQuestion;
                localOrder.customerName = ord.refToUser.firstName;
                localOrder.customerbuildingNumber = ord.addressBuildingNumber;
                localOrder.customerFloorNumber = ord.addressfloor;
                localOrder.customerHouseNumber = ord.addresshouseNumber;
                localOrder.customerLocationName = ord.addressLocationName;
                localOrder.customerId = ord.refToUser._id;
                localOrder.customerEmail = ord.refToUser.email;
                localOrder.customerPhoneNumber = ord.refToUser.phoneNumber;
                localOrder.bookingTime = ord.createdAt;
                localOrder.serviceTime = ord.serviceTimeSlot;
                localOrder.serviceDate = ord.serviceDate;
                localOrder.timeSlotCode = timeSlotCode;
                // localOrder.serviceTime = ord.serviceTime;
                localOrder.orderDescription = ord.orderDescription;
                localOrder.status = ord.status;
                localOrder.imageUrl = ord.imageUrl;
                if (typeof ord.refToServiceProvider != 'undefined') {
                    localOrder.serviceProviderId = ord.refToServiceProvider._id;
                    localOrder.serviceProviderName = ord.refToServiceProvider.firstName + ' ' + ord.refToServiceProvider.lastName;
                    localOrder.serviceProviderDescription = ord.refToServiceProvider.description;
                    localOrder.serviceProviderRating = ord.refToServiceProvider.refToRating.AverageRating;
                }
                orders.allOrders.push(localOrder);
            })
        }, function (err) {
            $timeout(function () {
            }, 2000)
        })
    }
    orders.fetchOrders();

    orders.showInfo = false;
    orders.showOrder = true;

    orders.openOrder = function (order) {

        orders.showOrder = true;
        orders.clicked = order._id;
        orders.showInfo = true;
        orders.veiwOrder = order;
        orders.idSelected = order._id;
    }

    orders.getBids = function (order) {
        orders.ordersBid = [];
        orders.showOrder = false;
        Bid.getBidsByOrder({orderId: order._id}, function (bids) {
            _.each(bids, function (bid) {
                if (typeof bid.bidding != 'undefined')
                    orders.ordersBid.push({
                        'organization': bid.refToServiceProvider.organization_name,
                        'name': bid.refToServiceProvider.firstName + ' ' + bid.refToServiceProvider.lastName,
                        'bidding': bid.bidding,
                        'bidWon': bid.bidWon
                    });
            })
            console.log(orders.ordersBid)
        })

    }


}
function registerServiceProvider(Upload, Category, User, ServiceProvider, $timeout, $q) {

    var RSPCtrl = this;
    RSPCtrl.submit = function () {


        if (RSPCtrl.file) {
            var promise = RSPCtrl.upload(RSPCtrl.file);
            promise.then(function (data) {
                console.log(data);
                RSPCtrl.user.imageURL = data;
            }).then(function () {

                User.registerServiceProviderAndEmployee({

                    user: RSPCtrl.user
                }, function (data) {

                    if (data.error) {
                        BootstrapDialog.show({
                            title: 'Register Service Provider Error',
                            message: data.error
                        });

                    }
                    else {

                        RSPCtrl.user = {};
                        RSPCtrl.submitted = false;

                        BootstrapDialog.show({
                            title: 'Register Service Provider',
                            message: 'Service Provider Registered'
                        });

                    }
                })
            })
        }
        else {
            RSPCtrl.fileRequired = true;
            $timeout(function () {
                RSPCtrl.fileRequired = false;
            }, 3000)

        }
    };


    Category.query(function (data) {
        RSPCtrl.category = [];
        _.each(data, function (d) {
            var service = [];
            _.each(d.service, function (ser) {


                service.push({serviceId: ser.serviceId, serviceType: ser.serviceType, checkBoxVal: false});

            })


            RSPCtrl.category.push({categoryId: d.categoryId, category: d.category, services: service});

        })


    }, function (err) {
        console.log(err);
        $timeout(function () {

        }, 2000)
    })

    RSPCtrl.user = {
        firstName: '',
        lastName: '',
        organization_name: '',
        photo: '',
        email: '',
        phoneNumber: '',
        password: '',
        accountType: 'serviceProvider',//PASS accountType as serviceProvider for registering a serviceProvider.
        refToServiceType: [],
        description: ''
    };

    RSPCtrl.addServiceType = function (service) {
        if (service.checkBoxVal) {
            RSPCtrl.user.refToServiceType.push(service.serviceId);
        }
        else {
            RSPCtrl.user.refToServiceType = _.reject(RSPCtrl.user.refToServiceType, function (ser) {
                return ser == service.serviceId;
            })
        }
    }


    // upload on file select or drop
    RSPCtrl.upload = function (file) {
        var deferred = $q.defer();
        Upload.upload({
            url: 'uploadImageBackend',
            data: {file: file}
        }).then(function (resp) {
            console.log(resp);
            deferred.resolve(resp.data);
            console.log('Success ' + resp.config.data.file.name + 'uploaded. Response: ' + resp.data);

            RSPCtrl.file = null;
        }, function (resp) {
            console.log('Error status: ' + resp.status);
        }, function (evt) {
            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
            console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
        });
        return deferred.promise;
    };


}
function registerEmployeeCtrl(Upload, Category, $rootScope, User, ServiceProvider, $timeout, $q) {

    var RECtrl = this;
    RECtrl.submit = function () {


        RECtrl.user.serviceProviderId = $rootScope.loggedInSP;


        if (RECtrl.file) {
            if(RECtrl.user.refToServiceType.length==0 || !RECtrl.user.refToServiceType){
                RECtrl.categoryRequired =true;
            }
            else {
                RECtrl.categoryRequired =false;
                RECtrl.fileRequired = false;
                var promise = RECtrl.upload(RECtrl.file);
                promise.then(function (data) {
                    RECtrl.user.imageURL = data;
                }).then(function () {

                    User.registerServiceProviderAndEmployee({

                        user: RECtrl.user
                    }, function (data) {

                        if (data.error) {
                            BootstrapDialog.show({
                                title: 'Register Employee Error',
                                message: data.error
                            });

                        }
                        else {

                            RECtrl.user = {};
                            RECtrl.submitted = false;
                            console.log(data);

                            BootstrapDialog.show({
                                title: 'Register Employee',
                                message: 'Employee Registered'
                            });

                        }
                    })

                })
            }
        }

        else {
            RECtrl.fileRequired = true;
            // $timeout(function () {
            //     RECtrl.fileRequired = false;
            // }, 3000)

        }


    };


    Category.query(function (data) {
        RECtrl.category = [];
        _.each(data, function (d) {
            var service = [];
            _.each(d.service, function (ser) {


                service.push({serviceId: ser.serviceId, serviceType: ser.serviceType, checkBoxVal: false});

            })


            RECtrl.category.push({categoryId: d.categoryId, category: d.category, services: service});

        })


    }, function (err) {
        console.log(err);
        $timeout(function () {

        }, 2000)
    })

    RECtrl.user = {
        firstName: '',
        lastName: '',
        organization_name: '',
        photo: '',
        email: '',
        phoneNumber: '',
        password: '',
        accountType: 'employee',//PASS accountType as serviceProvider for registering a serviceProvider.
        refToServiceType: [],
        description: ''
    };

    RECtrl.addServiceType = function (service) {
        if (service.checkBoxVal) {
            RECtrl.user.refToServiceType.push(service.serviceId);
        }
        else {
            RECtrl.user.refToServiceType = _.reject(RECtrl.user.refToServiceType, function (ser) {
                return ser == service.serviceId;
            })
        }
    }


    // upload on file select or drop
    RECtrl.upload = function (file) {
        var deferred = $q.defer();
        Upload.upload({
            url: 'uploadImageBackend',
            data: {file: file}
        }).then(function (resp) {
            console.log(resp);
            RECtrl.file = null;
            deferred.resolve(resp.data);
            console.log('Success ' + resp.config.data.file.name + 'uploaded. Response: ' + resp.data);
        }, function (resp) {
            console.log('Error status: ' + resp.status);
        }, function (evt) {
            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
            console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
        });
        return deferred.promise;
    };


}
function employeeCtrl($timeout, ServiceProvider, $rootScope, Category, Upload, User, Order, $q) {

    var empCtrl = this;
    empCtrl.showHistoryDetail = false;
    empCtrl.employeeDetails = true;
    empCtrl.showHistory = false;
    empCtrl.toggle = function () {
        empCtrl.employeeDetails = !empCtrl.employeeDetails;
        empCtrl.showHistory = !empCtrl.showHistory;

    }
    empCtrl.showProviderInfo = false;
    empCtrl.showEditForm = false;
    // console.log($rootScope.loggedInSP);
    ServiceProvider.allEmployee({refToSP: $rootScope.loggedInSP}, function (data) {
        empCtrl.allEmployees = [];

        console.log(data);
        _.each(data, function (provider) {
            console.log(provider)
            var localProvider = {};
            localProvider.employeeId = provider._id;
            localProvider.name = provider.firstName + ' ' + provider.lastName;
            localProvider.description = provider.description;
            // localProvider.photo = provider.refToUser._id;
            localProvider.totalRating = provider.refToRating.NumberOfRating;
            localProvider.averageRating = provider.refToRating.AverageRating;
            localProvider.accountActive = provider.accountActive;
            localProvider.imageURL = provider.imageURL;

            var localServiceOffered = [];
            _.each(provider.refToServiceType, function (service) {

                if (provider.refToServiceMenu.refToItem.length != 0)
                //localProvider.menuItem=provider.refToServiceMenu.refToItem;
                {
                    _.each(provider.refToServiceMenu.refToItem, function (item) {
                        if (service._id == item.serviceTypeId)
                            localServiceOffered.push({
                                "serviceTypeId": service._id,
                                "serviceType": service.serviceType,
                                "menu": {"priceEstimate": item.priceEstimate, "title": item.title}
                            })
                    })
                }
                else {

                    localServiceOffered.push({"serviceTypeId": service._id, "serviceType": service.serviceType})
                }

            })
            localProvider.serviceOffered = localServiceOffered;
            empCtrl.allEmployees.push(localProvider)
        })


    }, function (err) {
        $timeout(function () {
        }, 2000)
    })

    empCtrl.openProviderInfo = function (provider) {
        empCtrl.showProviderInfo = true;
        empCtrl.showEditForm = false;
        empCtrl.showHistoryDetail = false;
        empCtrl.employeeDetails = true;
        empCtrl.showHistory = false;
        empCtrl.veiwProvider = provider;
        ServiceProvider.getEmployee({empId: provider.employeeId}, function (data) {
            console.log(data);
            empCtrl.user = {
                userId: data.refToUser._id,
                serviceProviderId: data._id,
                firstName: data.firstName,
                lastName: data.lastName,
                organization_name: data.organization_name,
                photo: '',
                email: data.refToUser.email,
                phoneNumber: data.refToUser.phoneNumber,
                imageURL: data.imageURL,
                //password: '',
                accountType: 'serviceProvider',//PASS accountType as serviceProvider for registering a serviceProvider.
                refToServiceType: data.refToServiceType,
                description: data.description
            };


            _.each(empCtrl.category, function (cat) {
                _.each(cat.services, function (service) {
                    _.each(empCtrl.user.refToServiceType, function (serviceType) {
                        if (serviceType == service.serviceId)
                            service.checkBoxVal = true;

                    })

                })

            })
            console.log(empCtrl.user)
        }, function (err) {
            console.log(err);
        })
    }


    empCtrl.disableEmployee = function (employee) {
        employee.accountActive = !employee.accountActive
        ServiceProvider.updateAccountStatus
        ({refToSP: employee.employeeId, disable: employee.accountActive},
            function (data) {

                console.log(data);
            }, function (err) {
                console.log(err);
            })
    }

    Category.query(function (category) {
        empCtrl.category = [];

        _.each(category, function (d) {
            var service = [];
            _.each(d.service, function (ser) {
                service.push({serviceId: ser.serviceId, serviceType: ser.serviceType, checkBoxVal: false});
            })
            empCtrl.category.push({categoryId: d.categoryId, category: d.category, services: service});

        })

    }, function (err) {
        console.log(err);
        $timeout(function () {

        }, 2000)
    })


    function saveDetails() {

        User.editServiceProviderAndEmployee({

            user: empCtrl.user
        }, function (data) {
            console.log(data);

            BootstrapDialog.show({
                title: 'Edit Account',
                message: 'Changes saved'
            });

        })
    }

    empCtrl.submit = function () {
        if (empCtrl.file) {
            var promise = empCtrl.upload(empCtrl.file);
            promise.then(function (data) {
                empCtrl.user.imageURL = data;
            }).then(function () {
                saveDetails();
            })
        }
        else {
            saveDetails()
        }


    };

    empCtrl.workHistory = function (employee) {
        empCtrl.employeeAllOrder = [];

        Order.allEmployeeOrders({employeeId: employee.employeeId}, function (employeeOrders) {
            console.log(employeeOrders)

            _.each(employeeOrders, function (order) {
                var localOrder = {};
                if (order.refToServiceType.refToServiceCategory == '579735918c70e9cc338be339') {
                    localOrder.serviceCategory = 'Moving';
                }
                else if (order.refToServiceType.refToServiceCategory == '57974a17de6c152c3a191dee') {
                    localOrder.serviceCategory = 'Cleaning';
                }
                else {
                    localOrder.serviceCategory = 'Handyman';
                }
                localOrder.serviceType = order.refToServiceType.serviceType;
                localOrder.serviceDate = order.serviceDate;
                localOrder.serviceTimeSlot = order.serviceTimeSlot;
                localOrder.orderRating = order.orderRating;
                localOrder.customerFeedback = order.customerFeedback;
                localOrder.orderNo = order.orderNo;
                localOrder.employeeStartTime = order.employeeStartTime;
                localOrder.employeeStartEndTime = order.employeeStartEndTime;
                localOrder.employeeStartLocationLat = order.employeeStartLocationLat;
                localOrder.employeeStartLocationLong = order.employeeStartLocationLong;
                localOrder.employeeStartLocationEndLat = order.employeeStartLocationEndLat;
                localOrder.employeeStartLocationEndLong = order.employeeStartLocationEndLong;
                empCtrl.employeeAllOrder.push(localOrder);

            })
        }, function (err) {
            console.log(err)
        });
    }


    empCtrl.addServiceType = function (service) {
        if (service.checkBoxVal) {
            empCtrl.user.refToServiceType.push(service.serviceId);
        }
        else {
            empCtrl.user.refToServiceType = _.reject(empCtrl.user.refToServiceType, function (ser) {
                return ser == service.serviceId;
            })
        }
    }


    // upload on file select or drop
    empCtrl.upload = function (file) {
        var deferred = $q.defer();
        Upload.upload({
            url: 'uploadImageBackend',
            data: {file: file}
        }).then(function (resp) {
            console.log(resp);
            empCtrl.file = null;
            deferred.resolve(resp.data);
            console.log('Success ' + resp.config.data.file.name + 'uploaded. Response: ' + resp.data);
        }, function (resp) {
            console.log('Error status: ' + resp.status);
        }, function (evt) {
            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
            console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
        });
        return deferred.promise;
    };


}
function SPOrderCtrl($timeout, Order, Service, $rootScope, $location) {


    var orderFound = false;

    $rootScope.showHighlightedTab($location.path().replace('/', ''))
    var SPOrderCtrl = this;


    SPOrderCtrl.time = new Date();
    SPOrderCtrl.time;
    SPOrderCtrl.allOrders = [];
    SPOrderCtrl.clicked = '';


    SPOrderCtrl.reverse = true;
    SPOrderCtrl.sortBy = function (sortBy) {
        SPOrderCtrl.reverse = (SPOrderCtrl.sortOrderBy === sortBy) ? !SPOrderCtrl.reverse : false;
        SPOrderCtrl.sortOrderBy = sortBy;
    }
    SPOrderCtrl.sortOrderBy = 'orderNo';
    SPOrderCtrl.filterByCategory = 'all';
    SPOrderCtrl.filterByServiceDate = 'all';
    SPOrderCtrl.filterByStatus = 'all';

    var skip = 0;
    var limit = 10;
    SPOrderCtrl.allOrders = [];

    SPOrderCtrl.fetchOrders = function () {
        var allOrdersPromise = Order.spAllOrders({
            refToServiceProvider: $rootScope.loggedInSP,
            skip: skip,
            limit: limit
        }, function (data) {
            skip = data.length + skip;


            _.each(data, function (ord) {
                console.log(ord);
                console.log(ord.imageUrl)

                //inserting the code for timeslot for sorting purpose
                var timeSlotCode;
                switch (ord.serviceTimeSlot) {

                    case 'Morning (8AM-12PM)':
                        timeSlotCode = 1;
                        break;

                    case 'Afternoon (1PM-4PM)':

                        timeSlotCode = 2;
                        break;
                    case 'Evening (4PM-7PM)':

                        timeSlotCode = 3;
                        break;
                }
                var localOrder = {};
                var localQuestion = [];


                localOrder._id = ord._id;
                localOrder.orderNo = ord.orderNo;
                if (ord.refToServiceType.refToServiceCategory == '579735918c70e9cc338be339') {
                    localOrder.serviceCategory = 'Moving';
                }
                else if (ord.refToServiceType.refToServiceCategory == '57974a17de6c152c3a191dee') {
                    localOrder.serviceCategory = 'Cleaning';
                }
                else {
                    localOrder.serviceCategory = 'Handyman';
                }
                localOrder.serviceType = ord.refToServiceType.serviceType;
                localOrder.serviceTypeId = ord.refToServiceType._id;
                _.each(ord.refToServiceType.refToQuestionnaire.refToQuestion, function (ques) {
                    var localAnswer = [];
                    _.each(ques.refToAnswer, function (ans) {
                        _.each(ord.answerToQuestion, function (useranswer) {
                            if (ans._id.toString() == useranswer.toString()) {
                                localAnswer.push({'answerId': ans._id, 'answer': ans.choice});
                            }
                        })
                    })
                    localQuestion.push({"questionId": ques._id, "question": ques.question, "answer": localAnswer})
                })
                localOrder.questions = localQuestion;
                localOrder.customerName = ord.refToUser.firstName;
                localOrder.customerId = ord.refToUser._id;
                localOrder.customerEmail = ord.refToUser.email;
                localOrder.customerPhoneNumber = ord.refToUser.phoneNumber;
                localOrder.customerbuildingNumber = ord.addressBuildingNumber;
                localOrder.customerFloorNumber = ord.addressfloor;
                localOrder.customerHouseNumber = ord.addresshouseNumber;
                localOrder.customerLocationName = ord.addressLocationName;
                localOrder.bookingTime = ord.createdAt;
                localOrder.serviceTime = ord.serviceTimeSlot;
                localOrder.timeSlotCode = timeSlotCode;
                localOrder.serviceDate = ord.serviceDate;
                localOrder.orderDescription = ord.orderDescription;
                localOrder.status = ord.status;
                localOrder.imageUrl = ord.imageUrl;
                localOrder.employeeId = ord.employeeId;
                localOrder.orderRating = ord.orderRating;
                localOrder.customerFeedback = ord.customerFeedback;
                SPOrderCtrl.allOrders.push(localOrder);

                if ($rootScope.openThisOrder != null && $rootScope.openThisOrder == ord._id) {
                    console.log('inside check');
                    $rootScope.openThisOrder = null
                    orderFound = true;
                    SPOrderCtrl.openOrder(localOrder)
                }


            })
        }, function (err) {
            $timeout(function () {
            }, 2000)
        })


        allOrdersPromise['$promise'].then(function () {
            if ($rootScope.openThisOrder != null && !orderFound) {
                console.log('inside check');
                SPOrderCtrl.fetchOrders();
            }
        });
    }

    var timeout = 0;
    if (typeof $rootScope.loggedInSP == 'undefined') {
        timeout = 2000;
    }
    $timeout(function () {

        SPOrderCtrl.fetchOrders();
    }, timeout);


    SPOrderCtrl.showInfo = false;
    SPOrderCtrl.employeeList = false;

    SPOrderCtrl.openOrder = function (order) {
        SPOrderCtrl.clicked = order._id;
        SPOrderCtrl.showInfo = true;
        SPOrderCtrl.employeeList = false;
        SPOrderCtrl.veiwOrder = order;
        console.log(order)
    }
    SPOrderCtrl.assignEmployeeForOrderNum = '';

    var selectedOrder = {};
    SPOrderCtrl.selectEmployeeForOrder = function (order) {

        selectedOrder = order;
        SPOrderCtrl.chosenType = order.serviceType;
        SPOrderCtrl.serviceTime = '';
        SPOrderCtrl.serviceDate = order.serviceDate;

        SPOrderCtrl.assignEmployeeForOrderNum = order._id;
        SPOrderCtrl.showInfo = false;
        SPOrderCtrl.employeeList = true;

        var promise = Service.get({
            id: order.serviceTypeId
        }, function (data) {
            console.log(data);
            SPOrderCtrl.allEmployeesForTask = [];
            _.each(data.refToServiceProvider, function (SP) {
                if (SP.accountType == 'employee' && SP.serviceProviderId == $rootScope.loggedInSP) {
                    var localEmployee = {};
                    localEmployee.employeeId = SP._id;
                    localEmployee.name = SP.firstName + ' ' + SP.lastName;
                    localEmployee.phoneNumber = SP.refToUser.phoneNumber;
                    localEmployee.description = SP.description;
                    localEmployee.photo = SP.refToUser._id;
                    localEmployee.totalRating = SP.refToRating.NumberOfRating;
                    localEmployee.averageRating = SP.refToRating.AverageRating;
                    localEmployee.imageURL = SP.imageURL;

                    SPOrderCtrl.allEmployeesForTask.push(localEmployee)
                }
            })
        })
        promise['$promise'].then(function () {
            $timeout(function () {
                $(".time_element").timepicki();

            }, 500)
        })
        console.log(SPOrderCtrl.showInfo);
        console.log(SPOrderCtrl.employeeList)
    }

    SPOrderCtrl.assignOrderToEmployee = function (employee, index) {
        console.log(employee);
        console.log(index)


        if ($(".time_element").eq(index).val().indexOf('PM') == -1) {
            var hours = parseInt($(".time_element").eq(index).val().substring(0, 2)) + 12;
            var minutes = parseInt($(".time_element").eq(index).val().substring(3, 5));
        }
        else {
            var hours = parseInt($(".time_element").eq(index).val().substring(0, 2));
            var minutes = parseInt($(".time_element").eq(index).val().substring(3, 5));
        }

        employee.serviceTime = new Date();
        employee.serviceTime.setHours(hours);
        employee.serviceTime.setMinutes(hours);
        console.log(employee.serviceTime)


        var serviceDateAndTime = new Date(SPOrderCtrl.serviceDate.substring(0, 10) + employee.serviceTime.toISOString().substring(10, 25)).toISOString();

        Order.assignEmployeeAnOrder({
            orderId: SPOrderCtrl.assignEmployeeForOrderNum,
            employeeId: employee.employeeId,
            serviceDateAndTime: serviceDateAndTime,
        }, function (data) {
            console.log(data);

            _.each(SPOrderCtrl.allOrders, function (order) {
                if (order._id == SPOrderCtrl.assignEmployeeForOrderNum) {
                    order.employeeId = employee.employeeId;


                    SPOrderCtrl.showInfo = false;
                    SPOrderCtrl.employeeList = false;

                    BootstrapDialog.show({
                        message: 'Order Assigned successfully'
                    });
                }
            })

        }, function (err) {
            console.log(err);
        })
    }
}
function spAllBidsCtrl(Bid, $rootScope, $timeout, $location) {
    var spAllBids = this;
    spAllBids.spAllBids = [];
    spAllBids.clicked = '';
    var limit = 10;
    var skip = 0;

    spAllBids.fetchMoreBids = function () {

        Bid.allServiceProviderBids({
            'serviceProviderId': $rootScope.loggedInSP,
            limit: limit,
            skip: skip
        }, function (data) {
            skip = skip + data.length;
            _.each(data, function (bid) {
                var localOrder = {};
                var localQuestion = [];
                localOrder.bidId = bid._id;
                localOrder.bidding = bid.bidding;
                if (typeof bid.bidding == 'undefined')
                    localOrder.show = true;
                else
                    localOrder.show = false;
                localOrder.orderId = bid.refToOrder._id;
                localOrder.orderNo = bid.refToOrder.orderNo;

                localOrder.serviceType = bid.refToOrder.refToServiceType.serviceType;
                localOrder.serviceTypeId = bid.refToOrder.refToServiceType._id;
                localOrder.bidding = bid.refToOrder.estimateCost;
                if (bid.refToOrder.refToServiceType.refToServiceCategory == '579735918c70e9cc338be339') {
                    localOrder.serviceCategory = 'Moving';
                }
                else if (bid.refToOrder.refToServiceType.refToServiceCategory == '57974a17de6c152c3a191dee') {
                    localOrder.serviceCategory = 'Cleaning';
                }
                else {
                    localOrder.serviceCategory = 'Handyman';
                }
                _.each(bid.refToOrder.refToServiceType.refToQuestionnaire.refToQuestion, function (ques) {
                    var localAnswer = [];
                    _.each(ques.refToAnswer, function (ans) {
                        _.each(bid.refToOrder.answerToQuestion, function (useranswer) {
                            if (ans._id.toString() == useranswer.toString()) {
                                localAnswer.push({'answerId': ans._id, 'answer': ans.choice});
                            }
                        })
                    })
                    localQuestion.push({"questionId": ques._id, "question": ques.question, "answer": localAnswer})
                })
                localOrder.questions = localQuestion;
                localOrder.customerName = bid.refToOrder.refToUser.firstName;
                localOrder.customerId = bid.refToOrder.refToUser._id;
                localOrder.customerEmail = bid.refToOrder.refToUser.email;
                localOrder.customerPhoneNumber = bid.refToOrder.refToUser.phoneNumber;
                localOrder.bookingTime = bid.refToOrder.createdAt;
                localOrder.serviceTime = bid.refToOrder.serviceTimeSlot;
                localOrder.serviceDate = bid.refToOrder.serviceDate;
                localOrder.orderDescription = bid.refToOrder.orderDescription;
                localOrder.orderId = bid.refToOrder._id;
                localOrder.status = bid.refToOrder.status;
                localOrder.serviceProviderId = bid.refToServiceProvider;
                spAllBids.spAllBids.push(localOrder);
            })

        }, function (err) {
            console.log(err);
        })
    }

    var timeout = 0;
    if (typeof $rootScope.loggedInSP == 'undefined') {
        timeout = 2000;
    }
    $timeout(function () {

        spAllBids.fetchMoreBids();
    }, 2000)

    spAllBids.openBid = function (bid) {

        spAllBids.clicked = bid.bidId;
        spAllBids.showInfo = true;
        spAllBids.veiwOrder = bid;
        console.log(spAllBids.veiwOrder);
    }


    spAllBids.applyBid = function (bidding, bidId, orderId, serviceProviderId, customerId) {
        Bid.applyBid({
            bidId: bidId,
            bidding: bidding,
            orderId: orderId,
            serviceProviderId: serviceProviderId,
            customerId: customerId
        }, function (data) {

            $timeout(function () {
                $rootScope.openThisOrder = orderId;
                $location.path('/sporders')
            })
        })
    }
}
function editServiceProviderDetails(Upload, Category, User, ServiceProvider, $timeout, LoggedInUser, $q) {

    var ESPCtrl = this;
    ESPCtrl.category = [];
    Category.query(function (category) {


        _.each(category, function (d) {
            var service = [];
            _.each(d.service, function (ser) {
                service.push({serviceId: ser.serviceId, serviceType: ser.serviceType, checkBoxVal: false});
            })
            ESPCtrl.category.push({categoryId: d.categoryId, category: d.category, services: service});

        })


        LoggedInUser.get(function (data) {
            ESPCtrl.user = {
                userId: data.refToUser._id,
                serviceProviderId: data._id,
                firstName: data.firstName,
                lastName: data.lastName,
                organization_name: data.organization_name,
                photo: '',
                email: data.refToUser.email,
                phoneNumber: data.refToUser.phoneNumber,
                //password: '',
                accountType: 'serviceProvider',//PASS accountType as serviceProvider for registering a serviceProvider.
                refToServiceType: data.refToServiceType,
                description: data.description,
                imageURL: data.imageURL
            };


            _.each(ESPCtrl.category, function (cat) {
                _.each(cat.services, function (service) {
                    _.each(ESPCtrl.user.refToServiceType, function (serviceType) {
                        if (serviceType == service.serviceId)
                            service.checkBoxVal = true;

                    })

                })

            })
            console.log(ESPCtrl.user)

        }, function (err) {
            console.log(err);
        })


    }, function (err) {
        console.log(err);
        $timeout(function () {

        }, 2000)
    })

    function saveDetails() {

        User.editServiceProviderAndEmployee({

            user: ESPCtrl.user
        }, function (data) {
            console.log(data);

            BootstrapDialog.show({
                title: 'Edit Account',
                message: 'Changes saved'
            });

        })
    }


    ESPCtrl.submit = function () {

        if (ESPCtrl.file) {
            var promise = ESPCtrl.upload(ESPCtrl.file);
            promise.then(function (data) {
                ESPCtrl.user.imageURL = data;
            }).then(function () {

                saveDetails();
            })
        }
        else {
            saveDetails();
        }


    };


    ESPCtrl.addServiceType = function (service) {
        if (service.checkBoxVal) {
            ESPCtrl.user.refToServiceType.push(service.serviceId);
        }
        else {
            ESPCtrl.user.refToServiceType = _.reject(ESPCtrl.user.refToServiceType, function (ser) {
                return ser == service.serviceId;
            })
        }
    }


    // upload on file select or drop
    ESPCtrl.upload = function (file) {
        var deferred = $q.defer();
        Upload.upload({
            url: 'uploadImageBackend',
            data: {file: file}
        }).then(function (resp) {
            console.log(resp);
            deferred.resolve(resp.data);
            ESPCtrl.file = null;
            console.log('Success ' + resp.config.data.file.name + 'uploaded. Response: ' + resp.data);
        }, function (resp) {
            console.log('Error status: ' + resp.status);
        }, function (evt) {
            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
            console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
        });
        return deferred.promise;
    };


}
function customerListCtrl($timeout, User, Order) {
    console.log('customerListCtrl')

    var customerList = this;
    customerList.clicked = '';

    customerList.showProviderInfo = false;
    customerList.showProvider = true;
    User.customerList(function (data) {

        customerList.allCustomerList = [];

        _.each(data, function (customer) {
            var localCustomer = {};
            localCustomer.customerId = customer._id;
            localCustomer.name = customer.firstName;
            localCustomer.phoneNumber = customer.phoneNumber;
            localCustomer.email = customer.email;
            localCustomer.joinedOn = customer.createdAt;
            if (typeof customer.refToAddress != 'undefined') {
                localCustomer.address = {};
                localCustomer.address.addressBuildingNumber = customer.refToAddress.addressBuildingNumber;
                localCustomer.address.addressLocationName = customer.refToAddress.addressLocationName;
                localCustomer.address.addresshouseNumber = customer.refToAddress.addresshouseNumber;
                localCustomer.address.addressfloor = customer.refToAddress.addressfloor;
            }
            customerList.allCustomerList.push(localCustomer);


        })


    }, function (err) {
        $timeout(function () {
        }, 2000)
    })

    customerList.openCustomerOrderHistory = function (customer) {

        customerList.showProvider = true;
        customerList.clicked = customer.name;
        customerList.showProviderInfo = true;
        Order.getCustomerAllOrder({customerId: customer.customerId}
            , function (allOrders) {
                customerList.allOrders = [];
                _.each(allOrders, function (order) {
                    var localOrder = {};
                    var localQuestion = [];

                    localOrder.orderId = order._id;
                    localOrder.orderNo = order.orderNo;
                    localOrder.refToServiceProvider = order.refToServiceProvider;

                    localOrder.serviceType = order.refToServiceType.serviceType;
                    localOrder.serviceTypeId = order.refToServiceType._id;
                    if (order.refToServiceType.refToServiceCategory == '579735918c70e9cc338be339') {
                        localOrder.serviceCategory = 'Moving';
                    }
                    else if (order.refToServiceType.refToServiceCategory == '57974a17de6c152c3a191dee') {
                        localOrder.serviceCategory = 'Cleaning';
                    }
                    else {
                        localOrder.serviceCategory = 'Handyman';
                    }
                    _.each(order.refToServiceType.refToQuestionnaire[0].refToQuestion, function (ques) {
                        console.log(1)
                        var localAnswer = [];
                        _.each(ques.refToAnswer, function (ans) {

                            console.log(2)
                            _.each(order.answerToQuestion, function (useranswer) {
                                if (ans._id.toString() == useranswer.toString()) {
                                    localAnswer.push({'answerId': ans._id, 'answer': ans.choice});
                                    console.log(123)
                                }
                            })
                        })
                        localQuestion.push({"questionId": ques._id, "question": ques.question, "answer": localAnswer})
                    })

                    localOrder.questions = localQuestion;

                    localOrder.bookingTime = order.createdAt;
                    localOrder.serviceTime = order.serviceTimeSlot;
                    localOrder.serviceDate = order.serviceDate;
                    localOrder.orderDescription = order.orderDescription;
                    localOrder.status = order.status;
                    customerList.allOrders.push(localOrder);
                })

                console.log(customerList.allOrders)


            }, function (err) {
                console.log(err);
            })

    }

    //customerList.getAllEmployee=function(SPid) {
    //    customerList.showProvider=false;
    //    ServiceProvider.allEmployee({refToSP: SPid}, function (data) {
    //        customerList.allEmployees = [];
    //
    //        _.each(data, function (provider) {
    //            var localProvider = {};
    //            localProvider.providerId = provider._id;
    //            localProvider.name = provider.firstName + ' ' + provider.lastName;
    //            localProvider.description = provider.description;
    //            localProvider.photo = provider.refToUser._id;
    //            localProvider.totalRating = provider.refToRating.NumberOfRating;
    //            localProvider.averageRating = provider.refToRating.AverageRating;
    //
    //            var localServiceOffered = [];
    //            _.each(provider.refToServiceType, function (service) {
    //
    //                if (provider.refToServiceMenu.refToItem.length != 0)
    //                //localProvider.menuItem=provider.refToServiceMenu.refToItem;
    //                {
    //                    _.each(provider.refToServiceMenu.refToItem, function (item) {
    //                        if (service._id == item.serviceTypeId)
    //                            localServiceOffered.push({
    //                                "serviceTypeId": service._id,
    //                                "serviceType": service.serviceType,
    //                                "menu": {"priceEstimate": item.priceEstimate, "title": item.title}
    //                            })
    //                    })
    //                }
    //                else {
    //
    //                    localServiceOffered.push({"serviceTypeId": service._id, "serviceType": service.serviceType})
    //                }
    //
    //            })
    //            localProvider.serviceOffered = localServiceOffered;
    //            customerList.allEmployees.push(localProvider);
    //            console.log(customerList.allEmployees)
    //        })
    //
    //
    //    }, function (err) {
    //        $timeout(function () {
    //        }, 2000)
    //    })
    //}

}