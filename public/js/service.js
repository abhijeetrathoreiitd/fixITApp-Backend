angular.module('myApp')
    .factory('Category', function ($resource) {
        return $resource('category/:id'); // Note the full endpoint address
    })
    .factory('Service', function ($resource) {
        return $resource('service/:id'); // Note the full endpoint address
    })

    .factory('User', function ($resource) {
        return $resource('user/:id', null,
            {
                'update': {method: 'PUT'},
                'registerServiceProviderAndEmployee': {
                    method: 'POST',
                    url: '/user/registerServiceProviderAndEmployee',
                    isArray: false
                },
                'editServiceProviderAndEmployee': {
                    method: 'POST',
                    url: '/user/editServiceProviderAndEmployee',
                    isArray: false
                },
                'customerList': {
                    method: 'GET',
                    url: '/getCustomerList',
                    isArray: true
                },
                'forgotPassword': {
                    method: 'get',
                    url:'/user/forgotpassword/:email',
                    isArray: false
                }
            });
    })
    .factory('ServiceProvider', function ($resource) {
        return $resource('serviceProvider/:id', null,
            {
                'update': {method: 'PUT'},

                'updateAccountStatus': {
                    method: 'GET',
                    url: '/updateAccountStatusSP?refToSP=:refToSP&disable=:disable',
                    isArray: false
                },


                'allEmployee': {
                    method: 'GET',
                    url: '/serviceProvider/getAllEmployees/refToSP:refToSP',
                    params: {
                        'refToSP': 'refToSP'
                    },
                    isArray: true
                },

                'getEmployee': {
                    method: 'GET',
                    url: '/serviceProvider/employee/empId:empId',
                    params: {
                        'empId': 'empId'
                    },
                    isArray: false
                }

            }
        );
    })
    .factory('Login', function ($resource) {
        return $resource('login/:id'); // Note the full endpoint address
    })
    .factory('Order', function ($resource) {
        return $resource('/order?skip=:skip&limit=:limit', null,
            {
                'spAllOrders': {
                    method: 'POST',
                    url: '/order/getAllUserOrders',
                    isArray: true
                },

                'assignEmployeeAnOrder': {
                    method: 'PUT',
                    url: '/order/assignEmployee',
                    isArray: false
                },
                'getCustomerAllOrder': {
                    method: 'GET',
                    url: '/fetchCustomerAllOrders?customerId:=customerId',
                    isArray: true
                },
                'allEmployeeOrders': {
                    method: 'POST',
                    url: '/order/employeeOrders',
                    isArray: true
                }
            }); // Note the full endpoint address
    })
    .factory('LoggedInUser', function ($resource) {
        return $resource('LoggedInUserInfo/:id'); // Note the full endpoint address
    })
    .factory('Logout', function ($resource) {
        return $resource('logout/:id'); // Note the full endpoint address
    })
    .factory('Bid', function ($resource) {
        return $resource('/bid', null,
            {
                'allServiceProviderBids': {
                    method: 'GET',
                    url: '/bid?serviceProviderId=:serviceProviderId&skip=:skip&limit=:limit',
                    params: {
                        'serviceProviderId': 'serviceProviderId'
                    },
                    isArray: true
                },

                'applyBid': {
                    method: 'PUT',
                    url: '/bid/applyBid',
                    isArray: false
                },
                'getBidsByOrder': {
                    method: 'GET',
                    url: '/bid/getBidsForOrder/orderId:orderId',
                    params: {
                        'orderId': 'orderId'
                    },
                    isArray: true
                }

            }); // Note the full endpoint address
    })
    .factory('Time', function ($resource) {
        return $resource('time/:id'); // Note the full endpoint address
    })
    .factory("currentTime", function () {
        return {};
    });



