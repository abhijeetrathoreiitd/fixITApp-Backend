/**
 * Module dependencies
 */

var done = false;
var express = require('express'),
    bodyParser = require('body-parser'),
    session = require('express-session'),
    routes = require('./routes'),
    api = require('./routes/api'),
    http = require('http'),
    path = require('path'),
    logger = require('morgan'),
    fs = require('node-fs');

var multer = require('multer');

var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'public/uploads')
    },
    filename: function (req, file, cb) {
        cb(null, file.originalname)
    }
});

var upload = multer({storage: storage})
var mongoose = require('mongoose')
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
mongoose.connect('mongodb://localhost:27017/mongooseTest');


//Include Controllers Here
var UserController = require('./controllers/UserController');
var AddressController = require('./controllers/AddressController');
var ServiceCategoryController = require('./controllers/ServiceCategoryController');
var ServiceTypeController = require('./controllers/ServiceTypeController');
var QuestionnaireController = require('./controllers/QuestionnaireController');
var QuestionController = require('./controllers/QuestionController');
var AnswerController = require('./controllers/AnswerController');
var ServiceProviderController = require('./controllers/ServiceProviderController');
var ServiceMenuController = require('./controllers/ServiceMenuController');
var ItemController = require('./controllers/ItemController');
var OrderController = require('./controllers/OrderController');
var BidsController = require('./controllers/BidsController');
var EmailController = require('./controllers/EmailController');
var RatingController = require('./controllers/RatingController');


// Constants
var DEFAULT_PORT = 80;
var PORT = process.env.PORT || DEFAULT_PORT;

// App
var app = express();
app.set('views', __dirname + '/views');
//app.set('view engine', 'jade');
app.set('view engine', 'ejs');
app.set("jsonp callback", true);
app.set('jsonp callback name', 'code');
//app.use(logger('combined'));
app.use(function (req, res, next) {
    //console.log('%s %s %s', req.method, req.url, req.path);
    res.header('Access-Control-Allow-Credentials', true);
    res.header('Access-Control-Allow-Origin', "*");
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    if ('OPTIONS' == req.method) {
        res.sendStatus(200);
    }
    else {
        next();
    }
});
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(session({
    secret: 'keyboard cat',
    resave: false,
    saveUninitialized: true
}));
app.use(express.static(path.join(__dirname, 'public')));
//Configure passport middleware
app.use(passport.initialize());
app.use(passport.session());


//user signup strategy

passport.use('local-signup', new LocalStrategy({
        // by default, local strategy uses username and password, we will override with email
        usernameField: 'email',
        passwordField: 'password',
        passReqToCallback: true // allows us to pass back the entire request to the callback
    },
    UserController.userSignUp
));


//user login strategy
passport.use('local-login', new LocalStrategy(
    {
        usernameField: 'email',
        passReqToCallback: true // allows us to pass back the entire request to the callback
    },
    UserController.userLoginMechanism
));


passport.serializeUser(function (user, done) {
    done(null, user);
});

passport.deserializeUser(function (user, done) {
    done(null, user);
});

app.get('/logout', function (req, res) {
    req.logout();
    res.json({"status": "logout"});
});

/**
 * Routes
 */
app.options('*', function (req, res) {
    res.sendStatus(200);
});

// serve index and view partials
app.get('/', routes.index);
app.get('/partials/:name', ensureAuthenticated, routes.partials);
// JSON API
app.get('/api/name', api.name);
app.get('/logout', function (req, res) {
    req.logout();
    res.json({"status": "logout"});
});
app.post('/login', passport.authenticate('local-login'), function (req, res) {
    if (req) {
        res.json({"userData": req.user});
    }
    else {
        console.log("error")
    }
});


app.post('/user', passport.authenticate('local-signup'), function (req, res) {
    if (req) {
        res.json({"userData": req.user});
    }
    else {
        console.log("error")
    }
});


app.post('/user/registerServiceProviderAndEmployee', UserController.registerServiceProviderAndEmployee);
app.post('/user/editServiceProviderAndEmployee', UserController.editServiceProviderAndEmployee);
app.post('/user/fbSignUp', UserController.fbSignUp);
app.get('/user/forgotpassword/:email', UserController.forgotPassword);
app.put('/deviceToken', UserController.updateDeviceToken);

app.post('/uploadImageBackend', upload.single('file'), function (req, res, next) {
    var imageName = new Date().getTime();
    var path2 = 'public/uploads/' + imageName + path.extname(req.file.originalname)
    fs.rename(req.file.path, path2, function (data) {
        console.dir(data);
    })
    res.send(imageName + path.extname(req.file.originalname));

});

app.post('/uploadImage', upload.single('file'), function (req, res, next) {
    var imageName = new Date().getTime();
    var path2 = 'public/uploads/' + imageName + path.extname(req.file.originalname)
    fs.rename(req.file.path, path2, function (data) {
        console.dir(data);
    })
    res.send(imageName + path.extname(req.file.originalname));

});
app.put('/user', UserController.editUser);
app.post('/address', AddressController.addAddress);
//app.put('/address',AddressController.editAddress);
app.post('/questionnaire', QuestionnaireController.addQuestionnaire);
app.get('/questionnaire', QuestionnaireController.getQuestionnaire);
app.post('/question', QuestionController.addQuestion);
app.post('/answer', AnswerController.addAnswer);
app.get('/question', QuestionController.getQuestion);

//SERVICE PROVIDER CONTROLLER API's
app.post('/serviceProvider', ServiceProviderController.adminCreateServiceProvider);
app.get('/serviceProvider', ServiceProviderController.adminFetchAllServiceProvider);
app.put('/serviceProvider', ServiceProviderController.editServiceProvider);
app.get('/serviceProvider/getAllEmployees/refToSP:refToSP', ServiceProviderController.serviceProviderGetAllEmployee);
app.get('/serviceProvider/employee/empId:empId', ServiceProviderController.getEmployee);
app.get('/serviceProvider/getEmployee/refToSP:refToSP', ServiceProviderController.serviceProviderGetAllEmployee);
//app.get('/serviceProvider/delete/refToSP:refToSP',ServiceProviderController.deleteServiceProvider);
app.get('/updateAccountStatusSP', ServiceProviderController.updateAccountStatusSP);


app.get('/getCustomerList', UserController.adminFetchAllCustomer);


//SERVICE CONTROLLER API's
app.post('/service', ServiceTypeController.createServiceType);
app.get('/service/:id', ServiceTypeController.fetchServiceTypeWithId);
app.get('/service', ServiceTypeController.fetchAllServiceType);

//CATEGORY CONTROLLER API's
app.post('/category', ServiceCategoryController.createNewCategory);
app.get('/category', ServiceCategoryController.fetchAllCategory);

//BIDS CONTROLLER API's
app.post('/bid', BidsController.createNewBids);
app.get('/bid', BidsController.fetchServiceProviderBids);
app.put('/bid/applyBid', BidsController.createServiceProviderBid);
app.put('/bid/userSelectServiceProvider', BidsController.userAssignOrderToServiceProvider);
app.get('/bid/customerBids:refToUser', BidsController.fetchCustomerBids);
app.get('/bid/getBidsForOrder/orderId:orderId', BidsController.getBidsForOrder);


//ORDER CONTROLLER API's
app.post('/order', OrderController.createOrder);
app.get('/order', OrderController.adminFetchAllOrders);
app.get('/order/order', OrderController.getOrderWithId);
app.put('/order/cancelOrder', OrderController.cancelOrder);
app.post('/order/customerOrders', OrderController.getCustomersAllOrders);
app.post('/order/getAllUserOrders', OrderController.serviceProviderFetchAllOrders);
app.put('/order/assignEmployee', OrderController.assignEmployeeAnOrder);
app.put('/order/changeOrderStatus', OrderController.changeOrderStatus);
app.post('/order/employeeOrders', OrderController.getEmployeeOrders);
app.get('/fetchCustomerAllOrders', OrderController.fetchCustomerAllOrders);

//ORDER CONTROLLER API's
app.post('/rating', RatingController.rateService);
app.post('/serviceMenu', ServiceMenuController.addServiceMenu);
app.post('/item', ItemController.addItem);
app.post('/smsOTP', UserController.sendOTP);
app.post('/smsOTP/sendOTPPhoneChange', UserController.sendOTPPhoneChange);
app.get('/time', function (req, res) {
    res.json({"currentTime": new Date().toISOString()})
});
app.get('/loggedInUser/:refToUser', ServiceProviderController.getEmployeeInfoApp);
app.get('/LoggedInUserInfo', ServiceProviderController.getSPInfo);
app.get('/email/:email', EmailController.sendMail)


app.get('*', routes.index);
app.listen(PORT);
console.log('Running on http://localhost:' + PORT);

function ensureAuthenticated(req, res, next) {
    if (req.user)
        console.log(req.user.accountType)
    //case if user is loggedin and is trying to open login or sign up page,it redirect user to the respective home scrren
    if (req.isAuthenticated() && (req.path == "/partials/adminLogin" || req.path == "/partials/SPlogin")) {
        console.log(1)
        if (req.user.accountType == 'serviceProvider') {
            res.status(302);
            res.json({"redirect": "spAllBids"});
        }
        if (req.user.accountType == 'admin') {
            res.status(302);
            res.json({"redirect": "allOrders"});
        }
    }
    //case if user is not loggedin and is trying to open login or sign up page
    else if (!req.isAuthenticated() && (req.path == "/partials/adminLogin" || req.path == "/partials/SPlogin")) {
        console.log(2)
        return next();
    }


    else if (!req.isAuthenticated()) {
        console.log(3)
        res.status(302);
        res.json({"redirect": "spLogin"});
    }

    else if ((req.path == "/partials/addCategory" || req.path == "/partials/addService" || req.path == "/partials/orders" || req.path == "/partials/customerList" || req.path == "/partials/serviceProvider" || req.path == "/partials/registerServiceProvider")) {
        console.log(4)
        if (req.user.accountType == 'admin')
            return next();
        else {
            res.status(302);
            res.json({"redirect": "spLogin"});
        }
    }

    else if ((req.path == "/partials/spOrder" || req.path == "/partials/employees" || req.path == "/partials/registerEmployee" || req.path == "/partials/spAllBids" || req.path == "/partials/editServiceProviderDetails")) {
        console.log(5)
        if (req.user.accountType == 'serviceProvider')
            return next();
        else {
            res.status(302);
            res.json({"redirect": "spLogin"});
        }
    }
}

