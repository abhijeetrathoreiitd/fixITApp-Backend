var Item=require('../models/Item');
var ServiceMenu=require('../models/ServiceMenu');
var _ = require('underscore');
module.exports={
    addItem:addItem
}



//Insert new menu item for service provider
function addItem(req, res) {

    var serviceMenuId=req.body.serviceMenuId;
    var title=req.body.title;
    var priceEstimate=req.body.priceEstimate;
    var serviceTypeId=req.body.serviceTypeId;
    var item = new Item({
        refToServiceMenu:serviceMenuId,
        title:title,
        priceEstimate:priceEstimate,
        serviceTypeId:serviceTypeId
    });

    item.save(function (err, savedItem) {
        ServiceMenu.update(
            {_id: serviceMenuId},
            {$push: {'refToItem': savedItem._id}
            },
            {
                upsert: true
            }, function (err, doc) {
                Item
                    .find({_id:savedItem._id})
                    .exec(function (err, item) {
                        res.json(item)
                    });
            })
    });

};