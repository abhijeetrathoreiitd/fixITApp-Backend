var ServiceProvider = require('../models/ServiceProvider');
var Rating = require('../models/Rating');
var ServiceType = require('../models/ServiceType');
var ServiceMenu = require('../models/ServiceMenu');
var async = require('async')
var _ = require('underscore');
module.exports = {
    adminCreateServiceProvider: adminCreateServiceProvider,
    adminFetchAllServiceProvider: adminFetchAllServiceProvider,
    updateAccountStatusSP: updateAccountStatusSP,
    serviceProviderGetAllEmployee: serviceProviderGetAllEmployee,
    getEmployee: getEmployee,
    editServiceProvider: editServiceProvider,
    getEmployeeInfoApp: getEmployeeInfoApp,
    getSPInfo: getSPInfo

}


// Add New Service Provider Function Declaration
function adminCreateServiceProvider(req, res, userInfo, organizationName, description, refToServiceType, serviceProviderId) {

    var entryObject = {
        firstName: userInfo.firstName,
        lastName: userInfo.lastName,
        description: description,
        refToServiceType: refToServiceType,
        serviceProviderId: serviceProviderId,
        refToUser: userInfo._id,
        accountType: userInfo.accountType,
        organization_name: organizationName,
        phoneNumber: userInfo.phoneNumber,
        imageURL:userInfo.imageURL
    }
    var savedServiceProviderId;
    async.series([
        function (callback) {

            var serviceProvider = new ServiceProvider(entryObject);
            serviceProvider.save(function (err, savedServiceProvider) {
                savedServiceProviderId = savedServiceProvider._id;
                callback();
            })


        }, function (callback) {

            var rating = new Rating({
                refToServiceProvider: savedServiceProviderId,
            });


            rating.save(function (err, savedRating) {
                ServiceProvider.update(
                    {_id: savedServiceProviderId},
                    {
                        $set: {'refToRating': savedRating._id},

                    },
                    {upsert: true}, function (err, doc) {
                        callback();

                    })
            })

        }, function (callback) {

            var serviceMenu = new ServiceMenu({
                refToServiceProvider: savedServiceProviderId
            });
            serviceMenu.save(function (err, savedMenu) {
                ServiceProvider.update(
                    {_id: savedServiceProviderId},
                    {
                        $set: {refToServiceMenu: savedMenu._id}
                    },
                    {
                        upsert: true
                    }, function (err, doc) {

                        callback();

                    })
            });
        }, function (callback) {


            ServiceType.update(
                {_id: {$in: entryObject.refToServiceType}},
                {
                    $push: {'refToServiceProvider': savedServiceProviderId}

                },
                {
                    upsert: false,
                    multi: true
                }, function (err, doc) {
                    console.log('service type saved');
                    callback();
                })


        }, function () {


            ServiceProvider
                .findOne({_id: savedServiceProviderId})
                .deepPopulate('refToRating refToServiceType')
                .exec(function (err, user) {
                    res.json(user);

                })
        }])


}


// Get All Service Provider Function Declaration
function adminFetchAllServiceProvider(req, res) {
    ServiceProvider
        .find({'accountType': 'serviceProvider'})
        .deepPopulate('refToRating refToServiceMenu.refToItem refToServiceType refToUser')
        .exec(function (err, serviceProvider) {
            res.json(serviceProvider)
        });
};

function updateAccountStatusSP(req, res) {
    var refToSP = req.query.refToSP;
    var disable = req.query.disable;
    ServiceProvider.update({_id: refToSP},
        {
            $set: {accountActive: disable}
        },
        {upsert: false},
        function (err, result) {
            res.json(result);
        })
}


// Get Employees linked with SP  Function Declaration
function serviceProviderGetAllEmployee(req, res) {
    ServiceProvider
        .find({serviceProviderId: req.params.refToSP, 'accountType': 'employee'})
        .deepPopulate('refToRating refToServiceMenu.refToItem refToServiceType refToUser')
        .exec(function (err, serviceProvider) {
            res.json(serviceProvider)
        });
};


function getSPInfo(req, res) {

    if (typeof req.user != 'undefined')
        ServiceProvider
            .findOne({refToUser: req.user._id})
            .deepPopulate('refToUser')
            .exec(function (err, serviceProvider) {
                res.json(serviceProvider)
            });
};
// Get Employees linked with SP  Function Declaration
function getEmployeeInfoApp(req, res) {


    if (typeof req.params.refToUser != 'undefined')
        ServiceProvider
            .findOne({refToUser: req.params.refToUser})
            .deepPopulate('refToUser')
            .exec(function (err, serviceProvider) {
                res.json(serviceProvider)
            });
};

function editServiceProvider(req, res, serviceProvider) {

console.log(1);
    var pullArray = [];
    var pushArray = [];
    async.series([
        function (callback) {

            console.log(2);
            ServiceProvider.update(
                {_id: serviceProvider.serviceProviderId},
                {
                    $set: {
                        firstName: serviceProvider.firstName,
                        lastName: serviceProvider.lastName,
                        organization_name: serviceProvider.organization_name,
                        phoneNumber: serviceProvider.phoneNumber,
                        refToServiceType: serviceProvider.refToServiceType,
                        imageURL:serviceProvider.imageURL
                        
                    }

                },
                {upsert: true}, function (err, doc) {
                    callback();

                })
        }, function (callback) {

            console.log(3);

            ServiceType.find({refToServiceProvider: serviceProvider.serviceProviderId})
                .lean()
                .exec(function (err, results) {

                    console.log(4);
                    var plucked = [];

                    if (results != null) {
                        _.each(results, function (rslt) {
                            plucked.push(rslt._id.toString());

                        })
                    }

                    if (results != null) {
                        pullArray = _.difference(plucked, serviceProvider.refToServiceType);
                        pushArray = _.difference(serviceProvider.refToServiceType, plucked)
                    }
                    
                    console.log(pullArray)
                    console.log(pushArray)
                    callback();
                })
        }, function (callback) {

            console.log(5);

            ServiceType.update({_id: {$in: pushArray}},
                {
                    $push: {refToServiceProvider: serviceProvider.serviceProviderId},

                }, {multi: true}, function (err, result) {
                    console.log(err);
                    console.log(result);
                    callback();
                })
        }, function () {


            console.log(6);
            ServiceType.update({_id: {$in: pullArray}},
                {
                    $pullAll: {refToServiceProvider: [serviceProvider.serviceProviderId]},

                }, {multi: true}, function (err, result) {
                    console.log(err);
                    console.log(result);
                    res.json(result);
                })
        }])
}


function getEmployee(req, res) {
    ServiceProvider
        .findOne({_id: req.params.empId})
        .deepPopulate('refToUser')
        .exec(function (err, serviceProvider) {
            res.json(serviceProvider);
        });
};

