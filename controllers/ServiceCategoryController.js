var ServiceCategory=require('../models/ServiceCategory');
var _ = require('underscore');
module.exports={
    createNewCategory:createNewCategory,
    fetchAllCategory:fetchAllCategory
}



// Add New Category Function Declaration
function createNewCategory(req, res) {
    var category = req.body.category;
    var serviceCategory = new ServiceCategory({
        category: category
    });

    serviceCategory.save(function (err, result) {
        if (err) {
            console.log(err)
        }
        else {
            console.log('saved')
            res.json(result);
        }
    });
};

// Get All Category Function Declaration
function fetchAllCategory(req, res) {
    ServiceCategory
        .find()
        .deepPopulate('refToServiceType.refToQuestionnaire.refToQuestion.refToAnswer')
        .exec(function (err, serviceCategory) {
            var AllServiceCategory=[];
            _.each(serviceCategory,function(category){
                var localCategory={};
                localCategory.categoryId=category._id;
                localCategory.category=category.category;
                localCategory.service=[];

                _.each(category.refToServiceType,function(service){
                    var localService={};
                    localService.serviceId=service._id;
                    localService.serviceType=service.serviceType;
                    localService.order=service.order;
                    localService.disabled=service.disabled;
                    localService.cost=service.cost;
                    localService.questions=[];

                    _.each(service.refToQuestionnaire.refToQuestion,function(ques){
                        var localQuestions={};
                        localQuestions.questionId=ques._id;
                        localQuestions.question=ques.question;
                        localQuestions.questionType=ques.questionType;
                        localQuestions.answers=[];

                        _.each(ques.refToAnswer,function(ans){
                            var localAnswer={};
                            localAnswer.answerId=ans._id;
                            localAnswer.choice=ans.choice;
                            localQuestions.answers.push(localAnswer);
                        })
                        localService.questions.push(localQuestions);
                    })
                    localCategory.service.push(localService);
                })
                AllServiceCategory.push(localCategory)
            })
            res.json(AllServiceCategory);
        });
};