var UserController=require('../controllers/UserController');
var Rating=require('../models/Rating');
var Order=require('../models/Order');
var _ = require('underscore');
var async=require('async');
var qatarTime=require('../controllers/qatarTime');
module.exports={
    rateService:rateService
}


function rateService(req,res)
{

    var rating=req.body.rating;
    console.log('rating')
    console.dir(rating);


    async.series([
        function(callback)
        {
            if(typeof rating.serviceProviderId!='undefined') {
                Rating.findOne({'refToServiceProvider': rating.serviceProviderId}, function (err,SPResult) {
                    if (err)
                        console.log(err)
                    else {
                        console.log(1)
                        if(SPResult) {

                            console.log(2)
                            Rating.update({'_id': SPResult._id},
                                {
                                    $set: {
                                        AverageRating: (SPResult.AverageRating * SPResult.NumberOfRating + rating.customerRating) / (SPResult.NumberOfRating + 1),
                                        NumberOfRating: SPResult.NumberOfRating + 1
                                    }

                                }, {upsert: true}, function (spUpdate, err) {

                                    var message='You were rated '+rating.customerRating+' by '+ rating.customerName+' on '+qatarTime.qatarTime(new Date());
                                    var serviceProviderIdArray=[];
                                    serviceProviderIdArray.push(rating.serviceProviderId);
                                    console.log('sms'+message)
                                    UserController.getPhoneNumberDeviceTokenServiceProvider(serviceProviderIdArray,message,true,false);
                                    callback();
                                })
                        }
                        else
                            callback();

                    }

                })
            }
            else
                callback();
        },
        function(callback)
        {

            console.log(4)

            if(typeof rating.employeeId!='undefined') {

                console.log(5)
                Rating.findOne({'refToServiceProvider': rating.employeeId}, function (err,empResult) {
                    if (err)
                        console.log(err)
                    else {

                        console.log(6)
                        if(empResult) {

                            console.log(7)
                            Rating.update({'_id': empResult._id},
                                {
                                    AverageRating: (empResult.AverageRating * empResult.NumberOfRating + rating.customerRating) / (empResult.NumberOfRating + 1),
                                    NumberOfRating: empResult.NumberOfRating + 1

                                }, {upsert: true}, function (empUpdate, err) {
                                    callback();

                                    console.log('case2')
                                })
                        }
                        else {

                            console.log(8)
                            callback();
                        }
                    }

                })
            }
            else
                callback();
        }
        ,function(){
            Order.update(
                {_id: rating.orderId},
                {
                    customerFeedback:rating.customerFeedback,
                    orderRating:rating.customerRating
                },
                {
                    upsert: true
                }, function (err,result) {

                    console.log('case3')
                    res.json(result)
                })
        }
    ])
}



function qatarTime()
{

    var d = new Date();
    var date=new Date(d.getTime()+3*3600000);
    return date.toString().substring(0,24);
}