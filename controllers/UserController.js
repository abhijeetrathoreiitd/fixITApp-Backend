var twilioClient = require('twilio')('AC6528a86cd5eaf1c78320a79ff7c17e89', '76661d9963c3a5734e034b6540cf8ea0');
var User = require('../models/User');
var ServiceProvider = require('../models/ServiceProvider');
var PushNotificationController = require('../controllers/PushNotificationController');
var ServiceProviderController = require('../controllers/ServiceProviderController');
var bcrypt = require('bcryptjs')
var _ = require('underscore');
var api_key = 'key-55926f4a678c42180a02d8e58e7e7e82';
var domain = 'pixnary.com';
var mailgun = require('mailgun-js')({apiKey: api_key, domain: domain});

module.exports = {
    sendOTP: sendOTP,
    userSignUp: userSignUp,
    userLoginMechanism: userLoginMechanism,
    editUser: editUser,
    registerServiceProviderAndEmployee: registerServiceProviderAndEmployee,
    editServiceProviderAndEmployee: editServiceProviderAndEmployee,
    sendOTPPhoneChange: sendOTPPhoneChange,
    fbSignUp: fbSignUp,
    updateDeviceToken: updateDeviceToken,
    getPhoneNumberDeviceToken: getPhoneNumberDeviceToken,
    getPhoneNumberDeviceTokenServiceProvider: getPhoneNumberDeviceTokenServiceProvider,
    forgotPassword: forgotPassword,
    adminFetchAllCustomer: adminFetchAllCustomer
}

function sendOTP(req, res) {
    var phoneNumber = req.body.phoneNumber;
    var email = req.body.email;
    console.log(phoneNumber);
    var OTP = _.random(1000, 9999);
    console.log(OTP);


    User.findOne({email: email}, function (err, document) {
        if (!document) {

            //Send an SMS text message
            //TODO uncomment in production
            twilioClient.sendMessage({

                to: '+974'+phoneNumber, // Any number Twilio can deliver to
                from: '+12563443046', // A number you bought from Twilio and can use for outbound communication
                body: 'Your FXT verification code is ' + OTP + '. Use this one-time code to register.' // body of the SMS message

            }, function (err, responseData) { //this function is executed when a response is received from Twilio

                if (!err) { // "err" is an error received during the request, if any

                    // "responseData" is a JavaScript object containing data received from Twilio.
                    // A sample response from sending an SMS message is here (click "JSON" to see how the data appears in JavaScript):
                    // http://www.twilio.com/docs/api/rest/sending-sms#example-1

                    console.log(responseData.from); // outputs "+15097743610" our twilio number
                    console.log(responseData.body); // outputs "Testing twilio sms" the body of our sms

                }
            });
            res.json({'OTP': OTP});
        }
        else {
            res.json({error: 'userExists'});

        }
    })
}
function sendOTPPhoneChange(req, res) {
    var phoneNumber = req.body.phoneNumber;
    var email = req.body.email;
    console.log(phoneNumber);
    var OTP = _.random(1000, 9999);
    console.log(OTP);


    //Send an SMS text message
    //TODO uncomment in production
    twilioClient.sendMessage({

        to: '+974' + phoneNumber, // Any number Twilio can deliver to
        from: '+12563443046', // A number you bought from Twilio and can use for outbound communication
        body: 'Your FXT verification code is ' + OTP + '. Use this one-time code to change phone number.' // body of the SMS message

    }, function (err, responseData) { //this function is executed when a response is received from Twilio

        if (!err) { // "err" is an error received during the request, if any

            // "responseData" is a JavaScript object containing data received from Twilio.
            // A sample response from sending an SMS message is here (click "JSON" to see how the data appears in JavaScript):
            // http://www.twilio.com/docs/api/rest/sending-sms#example-1

            console.log(responseData.from); // outputs "+15097743610" our twilio number
            console.log(responseData.body); // outputs "Testing twilio sms" the body of our sms

        }
    });
    res.json({'OTP': OTP});


}
function userSignUp(req, email, password, done) {


    var user = req.body.user;

    var deviceToken = req.body.deviceToken;
    User.findOne({email: user.email}, function (err, document) {
        if (!document) {
            bcrypt.genSalt(10, function (err, salt) {
                if (err) return err;
                // hash the password using our new salt
                bcrypt.hash(user.password, salt, function (err, hash) {
                    if (err)
                        return done(null, false);
                    var userEntry = new User({
                        firstName: user.firstName,
                        email: user.email,
                        password: hash,
                        phoneNumber: user.phoneNumber,
                        accountType: user.accountType,
                        deviceToken: deviceToken
                    });
                    userEntry.save(function (err, details) {
                        if (err) {

                            console.log(err)
                        }
                        else {

                            console.log('saved');
                            //PushNotificationController.sendNotificationToCustomer(deviceToken)
                            return done(null, details);
                        }
                    })
                });
            });
        }
        else {

            console.log('user exists');
            return done(null, false);
        }
    })

};
function registerServiceProviderAndEmployee(req, res) {

    var user = req.body.user;
    console.dir(user);
    User.findOne({email: user.email}, function (err, document) {
        if (!document) {
            bcrypt.genSalt(10, function (err, salt) {
                if (err) return err;
                // hash the password using our new salt
                bcrypt.hash(user.password, salt, function (err, hash) {
                    if (err) res.json(err);
                    var userEntry = new User({
                        firstName: user.firstName,
                        email: user.email,
                        password: hash,
                        lastName: user.lastName,
                        phoneNumber: user.phoneNumber,
                        accountType: user.accountType,
                        imageURL:user.imageURL
                    });
                    console.log(userEntry);
                    userEntry.save(function (err, details) {
                        if (err) {

                            console.log(err)
                        }
                        else {


                            var message = {
                                from: 'FXT <no-reply@fxt.com>',
                                to: user.email,
                                subject: "FXT provider registration",
                                text: "FxT Home Services " +
                                "\n    ----------------------------------------" +
                                "\n\nYour FXT professional has registered you. Please note for login details." +
                                "\n\nEmail:" + user.email +
                                "\n\nPassword:" + user.password +
                                "\n\n   ----------------------------------------"
                            };

                            mailgun.messages().send(message, function (error, body) {
                                console.log('sending mail');
                            })
                            var customerIdArray = [];
                            customerIdArray.push(details._id);
                            var message = 'Your FXT professional has registered you. Please check your registered mail for login details.'
                            getPhoneNumberDeviceToken(customerIdArray, message, true, false);
                            console.log('spID')
                            console.log(user.serviceProviderId)
                            ServiceProviderController.adminCreateServiceProvider(req,res,details, user.organization_name, user.description, user.refToServiceType,user.serviceProviderId);
                        }
                    })
                });
            });
        }
        else {

            console.log('user exists')
            res.json({error: 'user exists'})
        }
    })

};
function editServiceProviderAndEmployee(req, res) {

    var user = req.body.user;
    console.dir(user);

    User.update(
        {_id: user.userId},
        {
            $set: {
                firstName: user.firstName,
                lastName: user.lastName,
                phoneNumber: user.phoneNumber
                
            }
        },
        {
            upsert: true
        }, function (err, doc) {
            if (err)
                console.log(err);
            else 
            {

                ServiceProviderController.editServiceProvider(req,res,user);
                
            }
        })

};
function userLoginMechanism(req, username, password, done) {

    console.log(username + ' ' + req.body.accountType + ' ' + password)
    User.findOne({email: username, accountType: req.body.accountType}).deepPopulate('refToAddress').exec(
        function (err, document) {
            if (err) {
                console.log(err)
                done(null, false);
            }
            else {
                if (document != null) {
                    bcrypt.compare(password, document.password, function (err, res) {
                        if (res) {
                            console.log("Logged in");
                            return done(null, document);

                        }
                        else {
                            console.log("Invalid Password");
                            return done(null, false);
                        }
                    });
                }
                else {
                    console.log("username does not exists ");
                    //return done(null, false);
                    return done(null, false);
                }

            }

        });

};
function editUser(req, res) {
    var details = req.body.details;
    var Case = req.body.case;
    //Case 1: edit name and email
    //Case 2: edit password
    //Case 3: edit number
    switch (Case) {
        case 1://Edit User name and email
            User.findOne({email: details.email, _id: {'$ne': details._id}},
                function (err, document) {
                    if (!document) {
                        User.update(
                            {_id: details._id},
                            {
                                $set: {
                                    email: details.email,
                                    firstName: details.firstName
                                }
                            },
                            {
                                upsert: true
                            }, function (err, doc) {
                                User.findOne({_id: details._id},
                                    function (err, document) {
                                        res.json(document);
                                    })
                            })
                    }
                    else {
                        res.json({data: 'Email Id Not Available'})

                    }
                })
            break;
        case 2:
            //Edit password
            var password = req.body.password;
            var newPassword = req.body.newPassword;

            User.findOne({_id: details._id},
                function (err, document) {

                    bcrypt.compare(password, document.password, function (err, resp) {
                        if (resp) {
                            console.log('1')
                            bcrypt.genSalt(10, function (err, salt) {
                                if (err) return err;
                                // hash the password using our new salt
                                bcrypt.hash(newPassword, salt, function (err, hash) {
                                    console.log('2')
                                    User.update(
                                        {_id: details._id},
                                        {
                                            $set: {
                                                password: hash
                                            }
                                        },
                                        {
                                            upsert: true
                                        }, function (err, doc) {
                                            console.log('3')
                                            User.findOne({_id: details._id},
                                                function (err, user) {
                                                    console.log(user)
                                                    res.json(user);
                                                })
                                        })
                                })
                            })
                        }
                        else {
                            console.log("Invalid Password");
                            res.json({data: 'Invalid Password'})
                        }
                    });
                })
            break;
        case 3:
            //Edit Phone Number
            var newPhoneNumber = req.body.phoneNumber;
            User.update(
                {_id: details._id},
                {
                    $set: {
                        phoneNumber: newPhoneNumber
                    },
                },
                {
                    upsert: true
                }, function (err, doc) {
                    console.log('3')
                    User.findOne({_id: details._id},
                        function (err, user) {
                            console.log(user)
                            res.json(user);
                        })
                })
            break;


    }


};
function fbSignUp(req, res) {

    var user = req.body.user;
    var deviceToken = req.body.deviceToken;
    console.dir(user);
    User.findOne({fbUserId: user.userId}).lean().deepPopulate('refToAddress').exec(function (err, document) {
        if (!document) {

            var userEntry = new User({
                firstName: user.name,
                email: user.email,
                accountType: 'customer',
                fbUserId: user.userId,
                deviceToken: deviceToken
            });
            userEntry.save(function (err, details) {
                if (err) {
                    res.json({'error': err})
                }
                else {
                    console.log('saved');

                    res.json({'userData': details})
                }
            })
        }
        else {

            res.json({'userData': document});
        }
    })

};
function updateDeviceToken(req, res) {
    var deviceToken = req.body.deviceToken;
    var userId = req.body.userId;
    User.update({_id: userId}, {deviceToken: deviceToken}, {upsert: true}, function (err, result) {
        console.log(result)
        res.json(result);
    })

}
function forgotPassword(req, res) {
    console.log(req.params.email);

    var newPassword = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < 5; i++)
        newPassword += possible.charAt(Math.floor(Math.random() * possible.length));


    var message = {
        from: 'FXT <no-reply@fxt.com>',
        to: req.params.email,
        subject: "Reset password request",
        text: "FxT Home Services " +
        "\n    ----------------------------------------" +
        "\n\nYour new password for FxT is :" + newPassword +
        "\n\nUse this password for logging in your account and change password on your next login." +
        "\n\n   ----------------------------------------" +
        "\n        Thanks for using FxT Services"
    };

    console.log(newPassword);

    bcrypt.genSalt(10, function (err, salt) {
        if (err) return err;
        // hash the password using our new salt
        bcrypt.hash(newPassword, salt, function (err, hash) {
            User.update({email: req.params.email}, {$set: {password: hash}}, {upsert: false},
                function (err, document) {
                    if (document.nModified == 1) {
                        mailgun.messages().send(message, function (error, body) {
                            console.log('sending mail');
                        })
                        res.json({data: 'done'});
                    }
                    else {

                        res.json({data: 'not done'})
                    }

                });
        })
    })


}
function adminFetchAllCustomer(req, res) {

    User.find({'accountType': 'customer'}).deepPopulate('refToAddress').lean().exec(function (err, result) {
        res.json(result);
    })
}


function getPhoneNumberDeviceToken(userId, message, sendSms, sendPush, state, stateParams) {


    User.find({_id: {$in: userId}}).lean().select('phoneNumber deviceToken').exec(function (err, result) {

        var deviceTokens = _.pluck(result, 'deviceToken');
        var phoneNumbers = _.pluck(result, 'phoneNumber');
        if (sendPush) {
            PushNotificationController.sendNotificationToCustomer(deviceTokens, message, state, stateParams);
        }
        if (sendSms) {
            smsNotification(phoneNumbers, message)
            console.log('sms true')
        }
    }, function (err) {
        console.dir(err)
    })

}


function getPhoneNumberDeviceTokenServiceProvider(userId, message, sendSms, sendPush, state, stateParams) {
    ServiceProvider.find({_id: {$in: userId}}).deepPopulate('refToUser').lean().exec(function (err, result) {

        var deviceTokens = [];
        var phoneNumbers = [];

        _.each(result, function (sp) {
            console.dir(sp);
            deviceTokens.push(sp.refToUser.deviceToken);
            phoneNumbers.push(sp.refToUser.phoneNumber);

        })

        if (sendPush) {
            PushNotificationController.sendNotificationToEmployee(deviceTokens, message, state, stateParams);
        }
        if (sendSms) {
            smsNotification(phoneNumbers, message);
        }
    }, function (err) {
        console.dir(err)
    })

}

function smsNotification(phoneNumbers, message) {

    _.each(phoneNumbers, function (phone) {
        //Send an SMS text message
        //TODO uncomment in production
        console.log('phone:' + phone)
        twilioClient.sendMessage({

            to: '+974' + phone, // Any number Twilio can deliver to
            from: '+12563443046', // A number you bought from Twilio and can use for outbound communication
            body: 'FXT:' + message // body of the SMS message

        }, function (err, responseData) { //this function is executed when a response is received from Twilio

            if (!err) { // "err" is an error received during the request, if any

                // "responseData" is a JavaScript object containing data received from Twilio.
                // A sample response from sending an SMS message is here (click "JSON" to see how the data appears in JavaScript):
                // http://www.twilio.com/docs/api/rest/sending-sms#example-1

                console.log(responseData.from); // outputs "+15097743610" our twilio number
                console.log(responseData.body); // outputs "Testing twilio sms" the body of our sms

            }
        });
    })
}





