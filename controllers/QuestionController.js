var Questionnaire=require('../models/Questionnaire');
var Question=require('../models/Question');
var _ = require('underscore');
module.exports={
    addQuestion:addQuestion,
    getQuestion:getQuestion

}



// Add New Question Function Declaration
function addQuestion(req, res) {

    var questionnaireId=req.body.questionnaireId;
    var ques=req.body.question;
    var questionType=req.body.questionType;

    var question = new Question({
        question:ques,
        refToQuestionnaire: questionnaireId,
        questionType:questionType// assign the _id from the person
    });
    question.save(function (err, savedQuestion) {
        Questionnaire.update(
            {_id: questionnaireId},
            {   $push: {'refToQuestion': savedQuestion._id}
            },
            {
                upsert: true
            }, function (err, doc) {
                Question
                    .find()
                    .deepPopulate('refToQuestionnaire.refToServiceType.refToServiceCategory')
                    .exec(function (err, question) {
                        res.json(question)
                    });
            })
    });
};


function getQuestion(req,res)
{

    Question
        .find()
        .deepPopulate('refToAnswer')
        .exec(function (err, question) {
            res.json(question)
        });
}