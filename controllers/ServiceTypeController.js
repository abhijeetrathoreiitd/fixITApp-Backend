var ServiceType=require('../models/ServiceType');
var ServiceCategory=require('../models/ServiceCategory');
var _ = require('underscore');
module.exports={
    createServiceType:createServiceType,
    fetchAllServiceType:fetchAllServiceType,
    fetchServiceTypeWithId:fetchServiceTypeWithId
}



// Add New Service Type Function Declaration
function createServiceType(req, res) {

    var serviceCategoryId = req.body.serviceCategoryId;
    var service = req.body.service;

    var serviceType = new ServiceType({
        serviceType: service,
        refToServiceCategory: serviceCategoryId     
    });
    serviceType.save(function (err, savedServiceType) {

        console.dir(savedServiceType)
        if(err)
            console.log(err);
        else {
            ServiceCategory.update(
                {_id: serviceCategoryId},
                {  $push: {'refToServiceType': savedServiceType._id}
                },
                {
                    upsert: true
                }, function (err, doc) {
                    ServiceCategory
                        .findOne()
                        .populate('refToServiceType')
                        .exec(function (err, serviceCategory) {
                            res.json(serviceCategory)
                        });
                })
        }
    });

};

// Get All Service Function Declaration
function fetchAllServiceType (req, res) {
    ServiceType.find()
        .deepPopulate('refToQuestionnaire.refToQuestion.refToAnswer')
        .exec(function (err, ServiceType) {
            res.json(ServiceType);
        });
};


// Get A Particular Service Function Declaration
function fetchServiceTypeWithId (req, res) {
    console.log(req.params.id)
    ServiceType.findOne({_id:req.params.id})
        .deepPopulate('refToServiceProvider.refToUser refToServiceProvider.refToRating')
        .exec(function (err, ServiceType) {

            console.log('service sent')
            res.json(ServiceType);
        });
};