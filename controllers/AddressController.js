var Address=require('../models/Address');
var User=require('../models/User');
var _ = require('underscore');
module.exports={addAddress:addAddress}

//Adds Address for new user
function addAddress(req,res) {
console.log(req.body)
    var userId = req.body.userId;
    var addressLat = req.body.address.addressLat;
    var addressLong = req.body.address.addressLong;
    var addressBuildingNumber = req.body.address.addressBuildingNumber;
    var addresshouseNumber = req.body.address.addresshouseNumber;
    var addressfloor = req.body.address.addressfloor;
    var addressLocationName = req.body.address.addressLocationName;
    var description = req.body.address.description;

    var address = new Address({
        addressLat: addressLat,
        addressLong: addressLong,
        addressBuildingNumber: addressBuildingNumber,
        addresshouseNumber: addresshouseNumber,
        addressfloor: addressfloor,
        addressLocationName: addressLocationName,
        description: description,
        refToUser: userId    // assign the _id from the person
    });

    Address.findOne({refToUser: userId},function(result){
        if(result==null)
        {
            console.log(1)
            address.save(function (err, savedAddress) {
                console.dir(err)
                User.update(
                    {_id: userId},
                    {
                       $set:{ 'refToAddress': savedAddress._id}

                    },
                    {upsert: true}, function (err, doc) {


                        console.log(savedAddress);
                        res.json(savedAddress)


                    }
                )
                console.log('saved')
            });

        }
        else{

            console.log(2)

            Address.update(
                {_id: result._id},
                {
                    $set: {
                        addressLat: addressLat,
                        addressLong: addressLong,
                        addressBuildingNumber: addressBuildingNumber,
                        addresshouseNumber: addresshouseNumber,
                        addressfloor: addressfloor,
                        addressLocationName: addressLocationName,
                        description: description,
                        refToUser: userId
                    }

                },
                {upsert: true}, function (err, doc) {


                    Address
                        .findOne({_id:result._id})
                        .exec(function (err, userAddress) {
                            console.log(userAddress);
                            res.json(userAddress)
                            // prints "The creator is Aaron"
                        });
                }
            )
        }
    })

}