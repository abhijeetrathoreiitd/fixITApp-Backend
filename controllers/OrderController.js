var twilioClient = require('twilio')('AC6528a86cd5eaf1c78320a79ff7c17e89', '76661d9963c3a5734e034b6540cf8ea0');
var Order=require('../models/Order');
var ServiceType=require('../models/ServiceType');
var ServiceSequence=require('../models/ServiceSequence');
var UserController=require('../controllers/UserController');
var _ = require('underscore');
var async=require('async');
var qatarTime=require('../controllers/qatarTime');
module.exports={
    createOrder:createOrder,
    adminFetchAllOrders:adminFetchAllOrders,
    serviceProviderFetchAllOrders:serviceProviderFetchAllOrders,
    getOrderWithId:getOrderWithId,
    assignEmployeeAnOrder:assignEmployeeAnOrder,
    getCustomersAllOrders:getCustomersAllOrders,
    cancelOrder:cancelOrder,
    getEmployeeOrders:getEmployeeOrders,
    changeOrderStatus:changeOrderStatus,
    fetchCustomerAllOrders:fetchCustomerAllOrders
}


//Creates new order
function createOrder  (req, res) {
    var OrderNo;
    var refToServiceType = req.body.refToServiceType;
    var answerToQuestion=req.body.answerToQuestion;
    var refToServiceProvider=req.body.serviceProviderId;
    var orderDescription=req.body.orderDescription;
    var refToUser=req.body.refToUser;
    var serviceDate=req.body.serviceDate;
    var serviceTimeSlot=req.body.serviceTimeSlot;
    var addressLat=req.body.addressLat;
    var addressLong=req.body.addressLong;
    var addressLatEnd=req.body.addressLatEnd;
    var addressLongEnd=req.body.addressLongEnd;
    var imageUrl=req.body.imageUrl;
    var addressBuildingNumber=req.body.addressBuildingNumber;
    var addresshouseNumber=req.body.addresshouseNumber;
    var addressfloor=req.body.addressfloor;
    var addressLocationName=req.body.addressLocationName;
    var addressBuildingNumberEnd=req.body.addressBuildingNumberEnd;
    var addresshouseNumberEnd=req.body.addresshouseNumberEnd;
    var addressfloorEnd=req.body.addressfloorEnd;
    var addressLocationNameEnd=req.body.addressLocationNameEnd;
    var addressLocationNameEnd=req.body.addressLocationNameEnd;
    var estimateCost=req.body.estimateCost;
    var request=req.body.request;


    ServiceSequence.findOneAndUpdate({}, {$inc: {"OrderNo": 1}}, function (err, results) {
        if (err) {
            console.log(err);
        }
        OrderNo = results.OrderNo;
        var order = new Order({
            orderNo:OrderNo,
            refToServiceType:refToServiceType,
            answerToQuestion:answerToQuestion,
            refToServiceProvider:refToServiceProvider,
            orderDescription:orderDescription,
            refToUser:refToUser,
            serviceDate:serviceDate,
            serviceTimeSlot:serviceTimeSlot,
            addressLat:addressLat,
            addressLong:addressLong,
            addressLatEnd:addressLatEnd,
            addressLongEnd:addressLongEnd,
            imageUrl:imageUrl,
            addressBuildingNumber:addressBuildingNumber,
            addresshouseNumber:addresshouseNumber,
            addressfloor:addressfloor,
            addressLocationName:addressLocationName,
            addressBuildingNumberEnd:addressBuildingNumberEnd,
            addresshouseNumberEnd:addresshouseNumberEnd,
            addressfloorEnd:addressfloorEnd,
            addressLocationNameEnd:addressLocationNameEnd,
            estimateCost:estimateCost,
            request:request
        });
        order.save(function (err, savedOrder) {
            res.json(savedOrder)
        });
    })
};

//Fetches all placed orders for Admin
function adminFetchAllOrders  (req, res) {

    var skip=req.query.skip;
    var limit=req.query.limit;
    Order
        .find().sort({ "createdAt": -1}).skip(skip).limit(limit)
        .deepPopulate('refToServiceProvider.refToRating refToServiceProvider.refToServiceMenu.refToItem ' +
            'refToServiceType.refToQuestionnaire.refToQuestion.refToAnswer refToUser.refToAddress'+
            'refToServiceType.refToServiceCategory')
        .exec(function (err, orders) {
            res.json(orders);
        });
};


//Fetches all placed orders by User
function getCustomersAllOrders(req,res)
{
    var customerOrderQuery=req.body.customerOrderQuery;
    Order
        .find(customerOrderQuery).sort({'createdAt':-1}).limit(3)
        .deepPopulate('refToServiceProvider.refToRating refToServiceProvider.refToServiceMenu.refToItem ' +
        'refToServiceType.refToQuestionnaire.refToQuestion.refToAnswer refToUser.refToAddress')
        .exec(function (err, orders) {
            var result
            if(orders.length>0)
            result={orders:orders,newOrderTimeStamp:new Date(),oldOrderTimeStamp:orders[orders.length-1].createdAt,loadMoreOldNews:true}
            else
             result={orders:orders,newOrderTimeStamp:new Date(),loadMoreOldNews:false}

            res.json(result);

        });
}



//Fetches all placed orders  assigned to employee
function getEmployeeOrders(req,res)
{
    Order
        .find({employeeId:req.body.employeeId})
        .deepPopulate('refToServiceProvider.refToRating refToServiceProvider.refToServiceMenu.refToItem ' +
        'refToServiceType.refToQuestionnaire.refToQuestion.refToAnswer refToUser.refToAddress employeeId').lean()
        .exec(function (err, orders) {
            res.json(orders);
        });
}



//Fetches all placed orders for ServiceProvider
function serviceProviderFetchAllOrders(req,res)
{
    console.log('refToServiceProvider'+req.body.refToServiceProvider)
    var skip=req.body.skip;
    var limit=req.body.limit;
    Order
        .find({refToServiceProvider:req.body.refToServiceProvider}).sort( { "createdAt": -1 } ).skip(skip).limit(limit)
        .deepPopulate('refToServiceProvider.refToRating refToServiceProvider.refToServiceMenu.refToItem ' +
        'refToServiceType.refToQuestionnaire.refToQuestion.refToAnswer refToUser.refToAddress employeeId')
        .exec(function (err, orders) {
            res.json(orders);
        });
}

function getOrderWithId(req,res)
{

    var orderId=req.query.orderId;

    Order.findOne({_id: orderId})
        .deepPopulate('refToServiceProvider ' +
        'refToServiceType.refToQuestionnaire.refToQuestion.refToAnswer ' +
        'refToUser.refToAddress').lean().exec(function(err,result)
        {
            res.json(result);
        })
}

function assignEmployeeAnOrder(req,res)
{
    Order.update(
        {_id: req.body.orderId},
        {
            $set:{employeeId:req.body.employeeId,serviceDateAndTime:req.body.serviceDateAndTime}
        },
        {
            multi:true
        }, function (err, doc) {
            Order.findOne({_id: req.body.orderId
                }).deepPopulate('refToUser refToServiceType').lean().exec(function (err, result) {



                    var EmployeeIdArray=[];
                    EmployeeIdArray.push(result.employeeId);
                    var stateParams=req.body.orderId;
                    var state='inbox';

                    var message='You have been assigned '+ result.refToServiceType.serviceType+' by '+result.refToUser.firstName+' for '+qatarTime.qatarTime(new Date(req.body.serviceDateAndTime)) +'. Check map for location details';
                    UserController.getPhoneNumberDeviceTokenServiceProvider(EmployeeIdArray,message,true,true,state,stateParams);
                    res.json(result)
                }
            )



        }
    )
}




function cancelOrder(req,res)
{
    var orderId=req.body.orderId;


    Order.update(
        {_id: orderId},
        {
            $set:{status:'cancelled'}
        },
        {
            upsert: true,
            multi:true
        }, function (err, doc) {
            Order.find({_id: orderId
                }, function (err, result) {
                    res.json(result)
                }
            )



        }
    )
}

function changeOrderStatus(req,res)
{
    var order=req.body.order;
    var updatedObj={};
    var message,messageSP;

    var state='tasks';
    var stateParams=order.orderId;
    if(order.status=='inProgress')
    {
        message='Your service provider is on their way! Please keep your phone close.';
        messageSP=order.employeeName+' has indicated they have started a job.';
        updatedObj.status=order.status;
        updatedObj.employeeStartTime=new Date().toISOString();
        updatedObj.employeeStartLocationLat=order.lat;
        updatedObj.employeeStartLocationLong=order.long;
    }
    else if(order.status=='completed')
    {
        message='Job Completed! Please rate your experience with '+order.employeeName;
        messageSP=order.employeeName+' has indicated they have ended a job.';
        updatedObj.status=order.status;
        updatedObj.employeeStartEndTime=new Date().toISOString();
        updatedObj.employeeStartLocationEndLat=order.lat;
        updatedObj.employeeStartLocationEndLong=order.long;
    }
    async.series([
        function(callback)
        {
    Order.update(
        {_id: order.orderId},

        updatedObj,
        {
         upsert: true
        }, function (err, doc) {
         callback()
       })
        },
        function()
        {
            Order.findOne({_id: order.orderId}).lean().exec(function(err,result)
            {

                var customerIdArray=[];
                customerIdArray.push(result.refToUser)
                var serviceProviderIdArray=[];
                serviceProviderIdArray.push(result.refToServiceProvider)

                UserController.getPhoneNumberDeviceTokenServiceProvider(serviceProviderIdArray,messageSP,true,false);
                UserController.getPhoneNumberDeviceToken(customerIdArray,message,true,true,state,stateParams);
                res.json(result);



            })
        }
    ])
};


function fetchCustomerAllOrders(req,res)
{
    var customerId=req.query.customerId;

    Order.find({refToUser: customerId})
        .deepPopulate('refToServiceProvider refToServiceType.refToQuestionnaire.refToQuestion.refToAnswer refToUser.refToAddress').lean().exec(function(err,result)
    {
        res.json(result);


    })
}