var mongoose = require('mongoose');
var Answer = require('../models/Answer');
var Question = require('../models/Question');
var _ = require('underscore');
module.exports = {
    addAnswer: addAnswer
}


//Add Answers to Question. Done By Admin
function addAnswer(req, res) {
    var questionId = req.body.questionId;
    var choice = req.body.choice;
    var hours = req.body.hours;
    var range = req.body.range;
    var baseRate = req.body.baseRate;

    var answer = new Answer({
        choice: choice,
        hours:hours,
        range:range,
        refToQuestion: questionId,     // assign the _id from the person
    });
    answer.save(function (err, savedAnswer) {
        console.log(err)
        Question.update(
            {_id: questionId},
            {
                $push: {'refToAnswer': savedAnswer._id}
            },
            {
                upsert: true
            }, function (err, doc) {
                Answer
                    .find({_id: savedAnswer._id})
                    .deepPopulate('refToQuestion.refToQuestionnaire.refToServiceType.refToServiceCategory')
                    .exec(function (err, answer) {
                        res.json(answer)
                    });
            })
    });
};