var Questionnaire=require('../models/Questionnaire');
var ServiceType=require('../models/ServiceType');
var _ = require('underscore');
module.exports={
    addQuestionnaire:addQuestionnaire,
    getQuestionnaire:getQuestionnaire
}





// Add New Questionnaire Function Declaration
function addQuestionnaire (req, res) {

    var serviceId = req.body.serviceId;

    var questionnaire = new Questionnaire({
        refToServiceType: serviceId     // assign the _id from the person
    });
    questionnaire.save(function (err, savedQuestionnaire) {
        ServiceType.update(
            {_id: serviceId},
            {  $push: {'refToQuestionnaire': savedQuestionnaire._id},
            },
            {
                upsert: true
            }, function (err, doc) {
                Questionnaire
                    .find()
                    .deepPopulate('refToServiceType.refToServiceCategory')
                    .exec(function (err, question) {
                        res.json(question)
                    });
            })
    });
};

// Get Questionnaire Function Declaration
function getQuestionnaire (req, res) {

    //var questionnaireId=req.body.questionnaireId;
    Questionnaire.
        find().deepPopulate('refToServiceType.refToServiceCategory')
        .exec(function (err, question) {
            res.json(question)
        })
};
