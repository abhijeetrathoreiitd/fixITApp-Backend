var Bids=require('../models/Bids');
var Order=require('../models/Order');
var UserController=require('../controllers/UserController');
var PushNotificationController = require('../controllers/PushNotificationController');
var _ = require('underscore');
var async=require('async');
var qatarTime=require('../controllers/qatarTime');
module.exports={
    createNewBids:createNewBids,
    fetchServiceProviderBids:fetchServiceProviderBids,
    createServiceProviderBid:createServiceProviderBidTemp,
    userAssignOrderToServiceProvider:userAssignOrderToServiceProvider,
    fetchCustomerBids:fetchCustomerBids,
    getBidsForOrder:getBidsForOrder
}


//Creates new row in bids table whenever an order is placed. 
function createNewBids(req, res) {
    var bidArray=req.body.bidArray;
    var customer=req.body.customerName;
    var serviceTypeName=req.body.serviceTypeName;
    console.log(serviceTypeName)
    console.log(customer)
    var serviceProviderIdArray= _.pluck(bidArray,'refToServiceProvider');

    console.log("++++++++++++++++++++++")
    console.dir(serviceProviderIdArray)
    console.log("++++++++++++++++++++++")
    Bids.insertMany(bidArray,function (err, details) {
            if (err) {

                //if a particular service is not provided by any provider then returns error
                console.log(err);
                res.json({error:err});
            }
            else {
                var message=customer +' has placed an order for '+ serviceTypeName+'. Check now to accept.';
                UserController.getPhoneNumberDeviceTokenServiceProvider(serviceProviderIdArray,message,true,false);
                res.json({details:details});
            }
        })
};


function fetchServiceProviderBids(req, res) {

    var skip=req.query.skip;
    var limit=req.query.limit;
    var serviceProviderId=req.query.serviceProviderId;
        Bids.find({refToServiceProvider:serviceProviderId,bidOver:false}).sort( { createdAt: -1 } ).skip(skip).limit(limit)
            .deepPopulate('refToOrder.refToServiceType.refToQuestionnaire.refToQuestion.refToAnswer refToOrder.refToUser')
            .exec(function (err, answer) {
                res.json(answer)
            });
};

function createServiceProviderBid(req,res){

    var bidId=req.body.bidId;
    var bidding=req.body.bidding;
    Bids.update(
        {_id: bidId},
        {
            $set: {
                bidding: bidding
            }
        },
        {upsert: true}, function (err, doc) {


            Bids
                .findOne({_id:bidId})
                .exec(function (err, result) {
                    res.json(result)
                });
        }
    )

}


function createServiceProviderBidTemp(req,res){

    var bidId=req.body.bidId;
    var bidding=req.body.bidding;
    var orderId=req.body.orderId;
    var serviceProviderId=req.body.serviceProviderId;
    var customerId=req.body.customerId;
    console.log('customerId'+customerId)





    async.series([
        function(callback)
        {
            Bids.update(
                {refToOrder: orderId},
                {
                    bidOver: true
                },
                {
                    upsert: true,
                    multi:true
                }, function (err, doc){
                    if(doc)
                    callback()
                })
        },
        function(callback)
        {

            Bids.update(
                {_id: bidId},
                {

                    bidding:bidding,
                    bidWon: true
                },
                {
                    upsert: true,
                    multi:true
                }, function (err, doc){
                    if(doc)
                        callback()
                })
        },
        function(callback){



            Order.update(
                {_id: orderId},
                {

                    refToServiceProvider: serviceProviderId,
                    status:'current'
                },
                {
                    upsert: true,
                    multi:true
                }, function (err, doc){
                    if(doc)
                        callback()
                })
        }
        ,function(){

            Order.findOne(
                {_id: orderId}).lean().exec(function (err, doc){

                    if(err)
                    console.log(err)
                    else {
                        var customerIdArray=[];
                        customerIdArray.push(customerId);
                        console.log('customer id')
                        console.dir(customerId);
                        console.log('customer id array')
                        console.dir(customerIdArray);
                        var message='Your job has been scheduled for '+qatarTime.qatarTime(doc.serviceDate).substring(0,15)+'. We will remind you when your service provider is on their way!';
                        console.log(message);
                        var state='tasks';
                        var stateParams=orderId;
                        UserController.getPhoneNumberDeviceToken(customerIdArray,message,false,true,state,stateParams);
                        res.json(doc);
                    }
                })
        }
    ])



}


function userAssignOrderToServiceProvider(req,res){

    var refToOrder=req.body.refToOrder;
    var serviceProviderId=req.body.serviceProviderId;

    Bids.update(
        {refToOrder: refToOrder},
        {

            bidOver: true
        },
        {
            upsert: true,
            multi:true
        }, function (err, doc) {

            Bids.update(
                {refToOrder: refToOrder,refToServiceProvider:serviceProviderId},
                {

                    bidWon: true
                },
                {
                    upsert: true,
                    multi:true
                }, function (err, doc) {


                    Order.update(
                        {_id: refToOrder},
                        {
                            refToServiceProvider:serviceProviderId,
                            status:'current'
                        },
                        {
                            upsert: true,
                            multi:true
                        }, function (err, doc) {


                            Order.find(                                {_id: refToOrder
                                }, function (err, result) {
   res.json(result)
                                })
                        })
               })
       })

}



function fetchCustomerBids(req, res) {

    var refToUser=req.params.refToUser;
    Bids.find({refToUser:refToUser})
        .deepPopulate('refToOrder.refToServiceType.refToQuestionnaire.refToQuestion.refToAnswer refToOrder refToServiceProvider')
        .exec(function (err, answer) {
            res.json(answer)
        });
};

function getBidsForOrder(req,res)
{
    Bids.find({refToOrder:req.params.orderId})
        .deepPopulate('refToServiceProvider')
        .exec(function (err, result) {
            res.json(result)
        })
}
