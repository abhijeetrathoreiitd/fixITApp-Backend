var ServiceMenu=require('../models/ServiceMenu');
var ServiceProvider=require('../models/ServiceProvider');
var _ = require('underscore');
module.exports={
    addServiceMenu:addServiceMenu
}

// Get Add Service Menu Function Declaration
function addServiceMenu (req, res) {

    var serviceProviderId=req.body.serviceProviderId;
    var serviceMenu = new ServiceMenu({
        refToServiceProvider:serviceProviderId
    });

    serviceMenu.save(function (err, savedMenu) {
        ServiceProvider.update(
            {_id: serviceProviderId},
            {
                $set: {
                    refToServiceMenu: savedMenu._id
                }
            },
            {
                upsert: true
            }, function (err, doc) {
                ServiceMenu
                    .find({_id:savedMenu._id})
                    .exec(function (err, menu) {
                        res.json(menu)
                    });
            })
    });

};