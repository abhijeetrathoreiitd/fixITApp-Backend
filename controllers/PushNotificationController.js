var ionicPushServer = require('ionic-push-server');

var customerCredentials = {
    IonicApplicationID: "e3c83ce5",
    IonicApplicationAPItoken: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiJmMDE1NWIyMy00ZTA5LTQ1MGUtOTFkNC03NTA5ZGY1NDA4MTcifQ.xIvImb_me-xbByjCvn3OZ3r-tz7QmsZj52jQHg-YznY"
};

var employeeCredentials = {
    IonicApplicationID: "5ea4b60e",
    IonicApplicationAPItoken: "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJlODBiOGM0MS00Y2U2LTQ3MzMtYWFmNy1jNzYyNmY0N2I3NmEifQ.c_TMqQFaupjhRYe6UziPOCo7sVc5noXOX8IpqAB4sNk"
};


module.exports={
    sendNotificationToEmployee:sendNotificationToEmployee,
    sendNotificationToCustomer:sendNotificationToCustomer
}

function sendNotificationToCustomer(allDeviceToken,message,state,stateParams) {

    var notification = {
        "tokens": allDeviceToken,
        "profile":"prod",
        "title": "FxT",
        "notification": {
            "message":message,
            "payload":{ "$state": state, "$stateParams": stateParams},
            "ios": {
                "title": "FxT",
                "message": message,
                "payload":{ "$state": state, "$stateParams": stateParams},
                "sound": "default",
                "priority": 10
            }
        }
    };
    ionicPushServer(customerCredentials, notification);

}



function sendNotificationToEmployee(allDeviceToken,message,state,stateParams) {

    console.log(allDeviceToken);
    var notification = {
        "tokens": allDeviceToken,
        "profile":"prod",
        "title": "FxT",
        "notification": {
            "message":message,
            "payload":{ "$state": state, "$stateParams": stateParams},
            "android": {
                "title": "FxT",
                "message": message,
                "payload":{ "$state": state, "$stateParams": stateParams},
                "image": "http://ec2-52-38-118-62.us-west-2.compute.amazonaws.com/img/push_icon.png",
                "style": "picture",
                "sound": "default",
                "icon":"push_icon"
            }
        }
    };
    ionicPushServer(employeeCredentials, notification);

}